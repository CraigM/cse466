import sys
import os
import time
os.chdir('./multiagent')
sys.path.append('.')

import pacman
import keyboardAgents
from game import Directions

import sensors

dirs=[Directions.NORTH,Directions.WEST,Directions.SOUTH,Directions.EAST]

dirIndex=1

threshold=10

def getTurn():
    """ return 1 to turn left, -1 to turn right, 0=no turn"""
    left=max(0,sensors.Sensor.sensors["1"].value)*3
    right=max(0,sensors.Sensor.sensors["0"].value)*5
    value=left-right
    print value,left,right
    if value>threshold: return 1
    if value<-threshold: return -1
    return 0
    

def getMove(self, legal):
    global dirIndex
    move = dirs[dirIndex%4]
    if move not in legal: move = Directions.STOP
    turn=getTurn()
    print ("right","straight","left")[turn+1]
    newDir=dirIndex+turn
    newMove=dirs[newDir%4]
    if newMove in legal or move == Directions.STOP:
        move=newMove
        dirIndex=newDir
    if move not in legal: move = Directions.STOP
    return move

keyboardAgents.KeyboardAgent.getMove=getMove
go=False
def callback():
    print "All calibrated!"
    global go
    go=True
sensors.begin_sensing(callback=callback)
while (not go):
    time.sleep(1)
args = pacman.readCommand( sys.argv[1:] ) # Get game components based on input
pacman.runGames( **args )