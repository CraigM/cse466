#include  "msp430F2013.h"

// The frequencies corresponding to different tones.
const unsigned int periods[] = { 8397, 7942, 7483, 7074, 6667, 6303, 5946, 5612,
		5301, 5000, 4721, 4453, 4206, 3971, 3747, 3536 };

volatile unsigned int period;

void main(void) {
	WDTCTL = WDTPW + WDTHOLD; // Stop WDT

	// Setup Pins : P1.0 in, rest out.
	P1DIR = ~0x01;
	P1OUT = 0x00;
	P2DIR = 0xFF;
	P2OUT = 0x00;

	// Setup PWM out on P1.3
	CCTL0 = CCIE; // CCR0 interrupt enabled
	P1SEL |= 0x0C; // P1.3 TA1/2 options
	period = 5000; // Start at 440
	CCR0 = period; // PWM Period.  Let's start off at A
	CCR1 = period >> 1; // CCR1 PWM duty cycle
	CCTL1 = OUTMOD_7; // CCR1 reset/set
	TACTL = TASSEL_2 + MC_1; // SMCLK, up mode

	// Set up the SD16 using P1.0
	SD16CTL = SD16REFON + SD16SSEL_1; // 1.2V ref, SMCLK
	SD16INCTL0 = SD16INCH_0; // SD16 Input Channel select A0
	SD16CCTL0 = SD16UNI + SD16IE; // 256OSR, unipolar, interrupt enable
	SD16AE = SD16AE0; // SD16 External Input Enable 0
	SD16CCTL0 |= SD16SC; // Set bit to start conversion

	_BIS_SR(LPM0_bits + GIE);
	// Enter LPM0
}

// SD16 interrupt service routine
#pragma vector = SD16_VECTOR
__interrupt void SD16ISR(void) {
	unsigned int temp = SD16MEM0;
	period = periods[temp >> 12];
}

// Timer A0 interrupt service routine
#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A(void) {
	CCR0 = period;
	CCR1 = period >> 1;
}

// Catch any unused interrupts as errors
#pragma vector=PORT2_VECTOR,PORT1_VECTOR,TIMER0_A1_VECTOR,WDT_VECTOR,NMI_VECTOR,USI_VECTOR
__interrupt void ISR_trap(void) {
	// the following will cause an access violation which results in a PUC reset
	WDTCTL = 0;
}
