#include  <msp430f2013.h>
#include "PIE/transmitter.h"
#include "PIE/flags.h"

volatile int transmitting = 1;
volatile int count = 0;

char *sentence = "Hello my name is Giganto";

void main(void) {
	// Disable the watchdog timer.
	WDTCTL = WDTPW + WDTHOLD;

	P1DIR = 0xFF;
	setup_transmit(BIT0);

	while (1) {
		P1OUT ^= BIT2;
		send_packet(sentence, 24);
	}
}
