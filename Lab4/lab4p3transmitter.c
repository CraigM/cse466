#include  <msp430f2013.h>
#include "PIE/transmitter.h"

volatile int transmitting = 1;
volatile int count = 0;

void main(void) {
	// Disable the watchdog timer.
	WDTCTL = WDTPW + WDTHOLD;

	P1DIR = 0xFF;
	setup_transmit(BIT0);

	while (1) {
		send_char('S');
	}
}
