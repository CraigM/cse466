#include  <msp430f2012.h>

#define NUM_SAMPS 20
#define COEFF -1
#define SAMPS_COUNTER 267


void main(void) {
	// Disable the watchdog timer.
	WDTCTL = WDTPW + WDTHOLD;

	// Use the fastest master clock.
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;

	// Set up TimerA to send signals to our ADC.
	TACTL |= TASSEL_2 | MC_1;
	TACCTL0 |= OUTMOD_3;
	TACCR1 = SAMPS_COUNTER;
	TACCR0 = SAMPS_COUNTER * 2;

	// Set up our pins.
	P1DIR = BIT0;

	// Set up our ADC.
	ADC10CTL0 |= ADC10ON | ADC10IE;
	ADC10CTL1 |= INCH_1 | SHS_2 | CONSEQ_2;
	ADC10DTC1 = NUM_SAMPS;
	ADC10AE0 = BIT1;

	int samples[NUM_SAMPS];

	// Begin!
	while (1) {
		ADC10SA = (unsigned int) samples;
		ADC10CTL0 |= ENC | ADC10SC;
		_BIS_SR(CPUOFF + GIE);
		ADC10CTL0 &= ~ENC;

		// First subtract the average, then run the samples (to avoid DC offset).
		// Setting the average to NUM_SAMPS / 2 helps get rid of division bias.
		int average = NUM_SAMPS / 2;
		int i;
		for (i = 0; i < NUM_SAMPS; i++) {
			average += samples[i];
		}
		average /= NUM_SAMPS;

		// Now that we have the average, run the samples.
		int q2 = 0, q1 = 0;
		for (i = 0; i < NUM_SAMPS; i++) {
			// Lose some granularity to prevent overflow.
			samples[i] = (COEFF * q1 - q2 + samples[i] - average) >> 2;
			q2 = q1;
			q1 = samples[i];
		}

		// Yes, I know about order of operations.  I just like parens.
		int magnitude = (q1 * q1) + (q2 * q2) - (q1 * q2 * COEFF);
		average = magnitude;

		if (magnitude > 800) {
			P1OUT |= BIT0;
		} else {
			P1OUT &= ~BIT0;
		}
	}
}

#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void) {
	LPM0_EXIT;
}
