#include <msp430f2012.h>
#include "PIE/receiver.h"

static char byte;
static char last_failure;
static volatile int error_code = 0;
static volatile int runs = 0;
static volatile int successful_runs = 0;
static volatile int error_runs = 0;
static volatile int incorrect_runs = 0;

void main(void) {
	// Disable the watchdog timer.
	WDTCTL = WDTPW + WDTHOLD;

	P1DIR = 0xFF;
	setup_receive();

	while (1) {
		error_code = get_byte(&byte);
		runs++;
		if (error_code) {
			P1OUT &= ~BIT0;
			error_runs++;
		} else if (byte == 'S') {
			P1OUT |= BIT0;
			successful_runs++;
		} else {
			P1OUT &= ~BIT0;
			incorrect_runs++;
			last_failure = byte;
		}
		if (runs == 0xFF) {
			runs = 0;
			successful_runs = 0;
			error_runs = 0;
			incorrect_runs = 0;
		}
	}
}
