#include  <msp430f2013.h>

void main(void) {
	// Disable the watchdog timer.
	WDTCTL = WDTPW + WDTHOLD;

	// Use the fastest master clock.
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;

	// Set up TimerA.
	CCTL0 = CCIE;
	CCR0 = 800;
	TACTL = TASSEL_2 + MC_1;

	// Set up our pins.
	P1DIR = 0xFF;

	_BIS_SR(CPUOFF + GIE);
}


#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A (void) {
	P1OUT ^= BIT0;
}
