#include  <msp430f2012.h>

void main(void) {
	// Disable the watchdog timer.
	WDTCTL = WDTPW + WDTHOLD;

	// Use the fastest master clock.
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;

	// Set up our pins.
	P1DIR = 0x00;

	_BIS_SR(CPUOFF + GIE);
}
