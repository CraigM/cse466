#include <msp430f2012.h>
#include "PIE/receiver.h"

static volatile char last_failure = 0;
static volatile int runs = 0;
static volatile int successful_chars = 0;
static volatile int crc_errors = 0;
static volatile int error_runs = 0;
static volatile int incorrect_chars = 0;

static void callback(char c) {
	// Do nothing! Our CRC will tell us if we read correctly.
}

void main(void) {
	// Disable the watchdog timer.
	WDTCTL = WDTPW + WDTHOLD;

	P1DIR = 0xFF;
	setup_receive();

	while (1) {
		int ret;
		if ((ret = read_packet((byte_callback*)&callback))) {
			P1OUT |= BIT0;
			if (ret == CRC_ERROR) {
				crc_errors++;
			} else {
				error_runs++;
			}
		} else {
			P1OUT &= ~BIT0;
		}
		runs++;

		if (runs == 0x64) {
			runs = 0;
			error_runs = 0;
			crc_errors = 0;
			successful_chars = 0;
			incorrect_chars = 0;
			last_failure = 0;
		}
	}
}
