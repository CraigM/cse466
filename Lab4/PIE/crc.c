#include "crc.h"

// Shamelessly borrowed from http://www.ccsinfo.com/forum/viewtopic.php?t=24977
unsigned int crc(unsigned int old_crc, char data) {
	int crc;
	int x;

	x = ((old_crc>>8) ^ data) & 0xFF;
	x ^= x >> 4;

	crc = (old_crc << 8) ^ (x << 12) ^ (x << 5) ^ x;

	return crc;
}
