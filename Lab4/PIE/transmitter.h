#ifndef PIE_TRANSMITTER_H_
#define PIE_TRANSMITTER_H_

#include "flags.h"

// Sets up the transmitter for sending with PIE.
void setup_transmit(char outpin);

// Sends the given byte, LSB first.
void send_char(char c);

// Sends a four bit flag. Zero is not a valid flag.
void send_flag(char c);

// Sends a single packet of length len. Returns 0 on success.
int send_packet(char *buf, unsigned int len);

#endif // PIE_TRANSMITTER_H_
