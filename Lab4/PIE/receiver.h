#ifndef PIE_RECEIVER_H_
#define PIE_RECEIVER_H_

#include "flags.h"

#define CRC_ERROR -1

void setup_receive(void);

// Returns 0 on success, negative values on failure, and positive values on
// flags (returns the value of the flag, flags cannot be 0). Puts result in c.
// Error codes:
//	-1:	timeout
int get_byte(char *c);

typedef void (*byte_callback(char));

// Reads a packet, calling byte_callback on each byte.
// Returns:
//	0 on success
//	-1 if the CRC does not match the data
//	1 if there is unexpected data received (such as calling read_packet in the
//    middle of a packet, or reading from a byte stream)
int read_packet(byte_callback cb);

#endif // PIE_RECEIVER_H_
