#include  <msp430f2013.h>
#include "transmitter.h"
#include "crc.h"

#define STATE_PREAMBLE 0
#define STATE_CHAR 1
#define STATE_NOP 2

#define CYCLES_PER_HALF_BIT 39
#define UP_CYCLES ((char_to_send & char_bit) ? CYCLES_PER_HALF_BIT : CYCLES_PER_HALF_BIT * 3)
#define PREAMBLE_BITS 4

static void send_helper(char c);

static int out_pin;
static int char_to_send;
static int char_bit;
static int transmitting;
static int duration;
static int state;
static int max_bits;

void setup_transmit(char outpin) {
	// Use the fastest master clock.
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;

	// Set up our pins.
	P1DIR |= outpin;
	out_pin = outpin;
}

int send_packet(char *buf, unsigned int len) {
	send_flag(FLAG_START);
	unsigned int i;
	unsigned int running_crc = 0;
	for (i = 0; i < len; i++) {
		running_crc = crc(running_crc, buf[i]);
	}
	for (i = 0; i < len; i++) {
		send_char(buf[i]);
	}
	send_flag(FLAG_CRC);
	send_char((char)running_crc);
	send_char((char)(running_crc >> 8));
	send_flag(FLAG_END);
	return 0;
}

void send_char(char c) {
	max_bits = 1 << 8;
	send_helper(c);
}

void send_flag(char c) {
	max_bits = 1 << 4;
	send_helper(c);
}

static void send_helper(char c) {
	// Set up TimerA.
	CCTL0 = CCIE;
	CCR0 = 800;
	TACTL = TASSEL_2 + MC_1;

	// And our globals.
	char_to_send = c;
	char_bit = 1;
	transmitting = 0;
	state = STATE_PREAMBLE;
	duration = 0;

	_BIS_SR(CPUOFF + GIE);

	// Disable clock interrupts when we're done to be good people :)
	CCTL0 &= ~CCIE;
}


#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A (void) {
	duration++;
	switch (state) {
	case STATE_PREAMBLE:
		P1OUT &= ~out_pin;
		if (duration == 0xFF) {
			duration = 0;
			transmitting = 1;
			state = STATE_CHAR;
		}
		break;
	case STATE_CHAR:
		if (transmitting) {
			// If we're in transmit mode, just toggle the pin and keep counting.
			P1OUT ^= out_pin;
			if (duration > UP_CYCLES) {
				duration = 0;
				transmitting = 0;
			}
		} else {
			// If we're not, keep the pin low.  We're sending the low part of
			// a bit.
			P1OUT &= ~out_pin;
			if (duration > CYCLES_PER_HALF_BIT) {
				duration = 0;
				transmitting = 1;
				char_bit = char_bit << 1;
				if (char_bit == max_bits) {
					// We've sent the entire byte.
					state = STATE_NOP;
				}
			}
		}
		break;
	case STATE_NOP:
		P1OUT &= ~out_pin;
		LPM0_EXIT;
	}
}
