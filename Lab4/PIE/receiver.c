#include  <msp430f2012.h>
#include "receiver.h"
#include "crc.h"

#define NUM_SAMPS 20
#define COEFF -1
#define SAMPS_COUNTER 267

#define MIN_PREAMBLE 6
#define ZERO_CUTOFF 4
#define TIMEOUT MIN_PREAMBLE

#define FLAG (1 << 4)

int samples[NUM_SAMPS];

void setup_receive() {
	// Use the fastest master clock.
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;

	// Set up TimerA to send signals to our ADC.
	TACTL |= TASSEL_2 | MC_1;
	TACCTL0 |= OUTMOD_3;
	TACCR1 = SAMPS_COUNTER;
	TACCR0 = SAMPS_COUNTER * 2;

	// Set up our pins.
	P1DIR &= ~BIT1;

	// Set up our ADC.
	ADC10CTL0 |= ADC10ON | ADC10IE;
	ADC10CTL1 |= INCH_1 | SHS_2 | CONSEQ_2;
	ADC10DTC1 = NUM_SAMPS;
	ADC10AE0 = BIT1;
}

int read_packet(byte_callback cb) {
	char c;
	int running_crc = 0;

	// Read the start flag.
	int ret = get_byte(&c);
	if (ret != FLAG_START) {
		return 1;
	}

	// Read all of our data bytes.
	while (!(ret = get_byte(&c))) {
		running_crc = crc(running_crc, c);
		cb(c);
	}

	// Read our CRC.
	if (ret != FLAG_CRC) {
		return 1;
	}
	if (get_byte(&c)) {
		return 1;
	}
	int crc = c;

	if (get_byte(&c)) {
		return 1;
	}
	crc |= (c << 8);

	// Read flag end.
	if (get_byte(&c) == FLAG_END) {
		// If we had a CRC mismatch, return -1.
		if (crc != running_crc) {
			return -1;
		}
		return 0;
	}

	return 1;
}

int get_byte(char *c) {
	char byte_started = 0;
	char duration = 0;
	char last = 0;
	char byte = 0;
	char pos = 1;

	while (1) {
		ADC10SA = (unsigned int) samples;
		ADC10CTL0 |= ENC | ADC10SC;
		_BIS_SR(CPUOFF + GIE);
		ADC10CTL0 &= ~ENC;

		// First subtract the average, then run the samples (to avoid DC offset).
		// Setting the average to NUM_SAMPS / 2 helps get rid of division bias.
		int average = NUM_SAMPS / 2;
		int i;
		for (i = 0; i < NUM_SAMPS; i++) {
			average += samples[i];
		}
		average /= NUM_SAMPS;

		// Now that we have the average, run the samples.
		int q2 = 0, q1 = 0;
		for (i = 0; i < NUM_SAMPS; i++) {
			// Lose some granularity to prevent overflow.
			samples[i] = (COEFF * q1 - q2 + samples[i] - average) >> 2;
			q2 = q1;
			q1 = samples[i];
		}

		// Yes, I know about order of operations.  I just like parens.
		int magnitude = (q1 * q1) + (q2 * q2) - (q1 * q2 * COEFF);
		average = magnitude;

		if (magnitude > 400) {
			// We have a high.

			if (byte_started) {
				if (last) {
					// We're continuing a high.
					duration++;
				} else {
					// We're starting a new high.
					duration = 0;
				}
			} else {
				if (duration > MIN_PREAMBLE) {
					duration = 0;
					byte_started = 1;
				} else {
					// We haven't received the appropriate length byte yet.
					duration = 0;
				}
			}

			last = 1;
		} else {
			// We have a low.

			if (byte_started) {
				if (last) {
					// We've just finished a high.
					if (duration < ZERO_CUTOFF) {
						byte |= pos;
					}
					duration = 0;
					pos = pos << 1;
					if (!pos) {
						// We've finished this byte.
						*c = byte;
						return 0;
					}
				} else {
					duration++;
					if (duration > TIMEOUT) {
						if (pos == FLAG) {
							return byte;
						}
						return -1;
					}
				}
			} else {
				duration++;
			}

			last = 0;
		}
	}
}

#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void) {
	LPM0_EXIT;
}
