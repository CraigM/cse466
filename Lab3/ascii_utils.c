#include  <msp430f2012.h>

#include "ascii_utils.h"
#include "UART/uart.h"

char *int_to_ascii(int val, char *buf) {
	unsigned int start = 0;
	unsigned int pos;

	// First check for negatives or zero.
	if (val < 0) {
		buf[start++] = '-';
		val = -val;
	} else if (val == 0) {
		buf[0] = '0';
		buf[1] = 0;
		return buf;
	}

	pos = start;
	while (val > 0) {
		buf[pos++] = val % 10 + '0';
		val = val / 10;
	}
	buf[pos--] = 0;

	while (start < pos) {
		char temp = buf[start];
		buf[start] = buf[pos];
		buf[pos] = temp;
		pos--;
		start++;
	}

	return buf;
}

void send_int(int i) {
	char buf[8];
	send_string(int_to_ascii(i, buf));
}


// Must be run as main.
static void time_conversion(void) {
	WDTCTL = WDTPW + WDTHOLD;
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;
	P1DIR = 0xFF;
	P1OUT = 0x00;
	P2DIR = 0xFF;
	P2OUT = 0x00;

	char stuff[8];
	int val = 0x2710;
	CCTL0 = CCIE;
	CCR0 = 0;
	TACTL = TASSEL_2 + MC_2;
	unsigned int start = TAR;
	int_to_ascii(val, stuff);
	unsigned int end = TAR;

	send_string("\r\nIt took ");
	send_string(int_to_ascii(end - start, stuff));
	send_string(" clock cycles to convert ");
	send_string(int_to_ascii(val, stuff));
	send_string(" to ASCII.");
}
