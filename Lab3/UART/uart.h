#ifndef UART_H_
#define UART_H_

// Assumes 16 MHz and a 2013 at 20 deg C.
#define BIT_TIME 1651

// Sends a single character through a serial connection with two stop bits and
// no parity.  Enables GIE and leaves it on at exit.
void send(char);

// Sends a complete C string.
void send_string(char *);

// Sends a complete C string followed by a newline.
void send_line(char *);

#endif /* UART_H_ */
