#include  <msp430f2012.h>

#include "uart.h"

// Which pin to output on.
const char OUTPUT_MASK = BIT2;

// The states in sending.
#define STATE_PREAMBLE 0
#define STATE_START_BIT 1
#define STATE_CHAR 2
#define STATE_STOP_BIT 3
#define STATE_POSTAMBLE 4
#define STATE_DONE 5

// The duration to stay in each state.
const int DURATIONS[5] = { 6, 1, 8, 2, 6 };

// The character we're sending, the current state, and how long we've been in
// the current state.
char char_to_send;
unsigned int state;
unsigned int duration;

void send_string(char *string) {
	char *c = string;
	while (*c) {
		send(*(c++));
	}
}

void send_line(char *string) {
	send_string(string);
	send_string("\r\n");
}

void send(char c) {
	// Set up TimerA.
	CCTL0 = CCIE;
	CCR0 = BIT_TIME;
	TACTL = TASSEL_2 + MC_2;

	// Set up our globals.
	char_to_send = c;
	state = STATE_PREAMBLE;
	duration = 0;

	_BIS_SR(CPUOFF + GIE);

	// Disable clock interrupts when we're done to be good people :)
	CCTL0 &= ~CCIE;
}

// Timer A0 interrupt service routine
#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A (void)
{
	duration++;
	CCR0 += BIT_TIME;	// VERY IMPORTANT
	switch (state) {
	case STATE_PREAMBLE:
		P1OUT |= OUTPUT_MASK;
		break;

	case STATE_START_BIT:
		P1OUT &= ~OUTPUT_MASK;
		break;

	case STATE_CHAR:
		if (char_to_send & (1 << (duration - 1))) {
			P1OUT |= OUTPUT_MASK;
		} else {
			P1OUT &= ~OUTPUT_MASK;
		}
		break;

	case STATE_STOP_BIT:
	case STATE_POSTAMBLE:
		P1OUT |= OUTPUT_MASK;
		break;
	}

	if (duration == DURATIONS[state]) {
		duration = 0;
		state++;
	}
	if (state == STATE_DONE) {
		LPM0_EXIT;
	}
}
