#include  <msp430f2012.h>

#include "ascii_utils.h"
#include "UART/uart.h"

#define NSAMPS 20

volatile int adc_value;

void main(void) {
	WDTCTL = WDTPW + WDTHOLD;

	// Setup Pins: all out (DIR doesn't matter for analog channel pins).
	P1DIR = 0xFF;
	P1OUT = 0x00;
	P2DIR = 0xFF;
	P2OUT = 0x00;

	// Use the fastest master clock.
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;

	char stuff[8];

	// Set ADC as input; configure ADC
	ADC10CTL0 |= ADC10ON | ADC10SHT_2 | ADC10IE;
	ADC10CTL1 |= ADC10DF | INCH_1;
	ADC10AE0 = BIT1;

	send_line("");
	send_line("Beginning conversion...");
	int total = 0;
	while (1) {
		//Look at SNR/update rate tradeoff
		int acc = 0; // acc should be a 16 bit variable
		int i;
		for (i = NSAMPS; i > 0; i--) {
			// Sample high...
			P1OUT |= BIT4;
			ADC10CTL0 |= ADC10SC | ENC;
			_BIS_SR(CPUOFF + GIE);
			adc_value = ADC10MEM / 256;
			acc += adc_value;

			// ...and sample low.
			P1OUT &= ~BIT4;
			ADC10CTL0 |= ADC10SC | ENC;
			_BIS_SR(CPUOFF + GIE);
			adc_value = ADC10MEM / 256;
			acc -= adc_value;
		}

		// Trust us, this is an IIR.
		total = acc / 4 + total / 4 * 3;
		send_line(int_to_ascii(total, stuff));
	}
}

#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void) {
	LPM0_EXIT;
}
