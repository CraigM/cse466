#include  <msp430f2012.h>

#include "ascii_utils.h"
#include "UART/uart.h"


#define NSAMPS 255

#define NUM_TX 2

int increment_lfsr(int);
static void increment(void);
static void output(void);
static void add_adc(int adc_value);

int tx[NUM_TX] = { 1, 127 };

int accum[NUM_TX];
int total[NUM_TX];

int output_bits[NUM_TX] = { BIT4, BIT5 };

void main(void) {
	WDTCTL = WDTPW + WDTHOLD;

	// Setup Pins: all out (DIR doesn't matter for analog channel pins).
	P1DIR = 0xFF;
	P1OUT = 0x00;
	P2DIR = 0xFF;
	P2OUT = 0x00;

	// Use the fastest master clock.
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;

	// Set ADC as input; configure ADC.
	ADC10CTL0 |= ADC10ON | ADC10SHT_2 | ADC10IE;
	ADC10CTL1 |= ADC10DF | INCH_1;
	ADC10AE0 = BIT1;

	// Clear out our totals array.
	int i;
	for (i = NUM_TX - 1; i >= 0; i--) {
		total[i] = 0;
	}

	send_line("");
	send_line("Beginning conversion...");
	while (1) {

		// Clear out our accumulation array.
		for (i = NUM_TX - 1; i >= 0; i--) {
			accum[i] = 0;
		}

		// Run NSAMPS samples.
		for (i = NSAMPS; i > 0; i--) {
			increment();

			output();

			ADC10CTL0 |= ADC10SC | ENC;
			_BIS_SR(CPUOFF + GIE);
			int adc_value = ADC10MEM / 512;

			add_adc(adc_value);
		}

		// Roll our accumulation array into our totals array.
		for (i = NUM_TX - 1; i >= 0; i--) {
			//total[i] = accum[i] / 16 + total[i] / 16 * 15;
			send_int(i);
			send_string(":");
			//send_int(total[i]);
			send_int(accum[i]);
			send_string("\t");
		}
		send_line("");
	}
}

// Increments the given value according to which step it corresponds to in a
// 8 bit LFSR.
inline int increment_lfsr(int input) {
	return ((input >> 1) ^ (-(input & 1u) & 0xB8u)) & 255;
}

// Increments the LFSR registers.
inline static void increment() {
	int j;
	for (j = NUM_TX - 1; j >= 0; j--) {
		tx[j] = increment_lfsr(tx[j]);
	}
}

// Sets the outputs.
inline static void output() {
	int j;
	for (j = NUM_TX - 1; j >= 0; j--) {
		if (tx[j] & 1) {
			// Output high
			P1OUT |= output_bits[j];
		} else {
			// Output low
			P1OUT &= ~output_bits[j];
		}
	}
}

// Adds the adc value to the accumulation registers.
inline static void add_adc(int adc_value) {
	int j;
	for (j = NUM_TX - 1; j >= 0; j--) {
		if(tx[j] & 1) {
			accum[j] += adc_value;
		} else {
			accum[j] -= adc_value;
		}
	}
}

#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void) {
	LPM0_EXIT;
}
