/*----------------------------------------------------------------------------
 *  Demo Application for SimpliciTI 
 * 
 *  L. Friedman 
 *  Texas Instruments, Inc.
 *---------------------------------------------------------------------------- */

/********************************************************************************************
 Copyright 2007-2009 Texas Instruments Incorporated. All rights reserved.

 IMPORTANT: Your use of this Software is limited to those specific rights granted under
 the terms of a software license agreement between the user who downloaded the software,
 his/her employer (which must be your employer) and Texas Instruments Incorporated (the
 "License"). You may not use this Software unless you agree to abide by the terms of the
 License. The License limits your use, and you acknowledge, that the Software may not be
 modified, copied or distributed unless embedded on a Texas Instruments microcontroller
 or used solely and exclusively in conjunction with a Texas Instruments radio frequency
 transceiver, which is integrated into your product. Other than for the foregoing purpose,
 you may not use, reproduce, copy, prepare derivative works of, modify, distribute,
 perform, display or sell this Software and/or its documentation for any purpose.

 YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS�
 WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY
 WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
 IN NO EVENT SHALL TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
 NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE
 THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY
 INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST
 DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY
 THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

 Should you have any questions regarding your right to use this Software,
 contact Texas Instruments Incorporated at www.TI.com.
 **************************************************************************************************/

#include "bsp.h"
#include "mrfi.h"
#include "nwk_types.h"
#include "nwk_api.h"
#include "bsp_leds.h"
#include "bsp_buttons.h"

#include "app_remap_led.h"
#include <msp430f2274.h>

static void linkTo(void);

void toggleLED(uint8_t);

static uint8_t sTxTid = 0, sRxTid = 0;
static uint8_t msg[2];
static linkID_t sLinkID1 = 0;

#define STATIC_PORT 0x3E
const addr_t ADDRESSES[2] = {
		{0x15, 0x15, 0x15, 0x15},
		{0x16, 0x16, 0x16, 0x16}
};
const int us = 0;

/* application Rx frame handler. */
static uint8_t sRxCallback(linkID_t);

#define SPIN_ABOUT_A_SECOND  NWK_DELAY(1000)

void main(void) {
	BSP_Init();

	/* If an on-the-fly device address is generated it must be done before the
	 * call to SMPL_Init(). If the address is set here the ROM value will not
	 * be used. If SMPL_Init() runs before this IOCTL is used the IOCTL call
	 * will not take effect. One shot only. The IOCTL call below is conformal.
	 */
#ifdef I_WANT_TO_CHANGE_DEFAULT_ROM_DEVICE_ADDRESS_PSEUDO_CODE
	{
		addr_t lAddr;

		createRandomAddress(&lAddr);
		SMPL_Ioctl(IOCTL_OBJ_ADDR, IOCTL_ACT_SET, &lAddr);
	}
#endif /* I_WANT_TO_CHANGE_DEFAULT_ROM_DEVICE_ADDRESS_PSEUDO_CODE */

	/* This call will fail because the join will fail since there is no Access Point
	 * in this scenario. But we don't care -- just use the default link token later.
	 * We supply a callback pointer to handle the message returned by the peer.
	 */
	SMPL_Ioctl(IOCTL_OBJ_ADDR, IOCTL_ACT_SET, (void*) &(ADDRESSES[us]));
	SMPL_Init(sRxCallback);

	/* turn on LEDs. */
	if (!BSP_LED2_IS_ON()) {
		toggleLED(2);
	}
	if (!BSP_LED1_IS_ON()) {
		toggleLED(1);
	}

	/* wait for a button press... */
	do {
		if (BSP_BUTTON1() || BSP_BUTTON2()) {
			break;
		}
	} while (1);

	/* never coming back... */
	linkTo();

	/* but in case we do... */
	while (1)
		;
}

static void linkTo() {
	while (SMPL_SUCCESS
			!= SMPL_Commission((void*) &(ADDRESSES[(us+1)%2]), STATIC_PORT, STATIC_PORT,
					&sLinkID1)) {
		/* blink LEDs until we link successfully */
		toggleLED(1);
		toggleLED(2);
		SPIN_ABOUT_A_SECOND;
	}
	// Turn off our LEDs
	if (BSP_LED2_IS_ON()) {
		toggleLED(2);
	}
	if (BSP_LED1_IS_ON()) {
		toggleLED(1);
	}

	/* turn on RX. default is RX off. */
	SMPL_Ioctl(IOCTL_OBJ_RADIO, IOCTL_ACT_RADIO_RXON, 0);

	// Activate our UART
	WDTCTL = WDTPW + WDTHOLD; // Stop WDT
	BCSCTL1 = CALBC1_1MHZ; // Set DCO
	DCOCTL = CALDCO_1MHZ;
	P3SEL = 0x30; // P3.4,5 = USCI_A0 TXD/RXD
	UCA0CTL1 |= UCSSEL_2; // SMCLK
	UCA0BR0 = 104; // 1MHz 9600
	UCA0BR1 = 0; // 1MHz 9600
	UCA0MCTL = UCBRS0; // Modulation UCBRSx = 1
	UCA0CTL1 &= ~UCSWRST; // **Initialize USCI state machine**
	IE2 |= UCA0RXIE; // Enable USCI_A0 RX interrupt

	while (1) {
		__bis_SR_register(LPM0_bits + GIE);
		toggleLED(2);
		SMPL_Send(sLinkID1, msg, 2);
	}
	// Enter LPM0, interrupts enabled
}

void toggleLED(uint8_t which) {
	if (1 == which) {
		BSP_TOGGLE_LED1();
	} else if (2 == which) {
		BSP_TOGGLE_LED2();
	}
	return;
}

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void) {
	// When we receive a byte over serial, send it over wireless.
	msg[0] = UCA0RXBUF;
	msg[1] = sTxTid++;
	LPM0_EXIT;
}

/* handle received frames. */
static uint8_t sRxCallback(linkID_t port) {
	uint8_t msg[2], len, tid;

	/* is the callback for the link ID we want to handle? */
	if (port == sLinkID1) {
		/* yes. go get the frame. we know this call will succeed. */
		if ((SMPL_SUCCESS == SMPL_Receive(sLinkID1, msg, &len)) && len) {
			/* Check the application sequence number to detect
			 * late or missing frames...
			 */
			tid = *(msg + 1);
			if ((tid && tid > sRxTid) || (!tid && sRxTid)) {
				if (tid > sRxTid) {
					// Spin until we can send over serial, then send the received byte.
					while (!(IFG2 & UCA0TXIFG))
						;
					UCA0TXBUF = msg[0];
					sRxTid = tid;
					toggleLED(1);
				}
			}
			/* drop frame. we're done with it. */
			return 1;
		}
	}
	/* keep frame for later handling. */
	return 0;
}

