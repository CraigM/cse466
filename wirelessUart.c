// Alexandre Bykov
// Sean Cowan
// CSE 466
// Lab 5
//
// Bidirectional wireless UART implementation

#include "msp430f2274.h"
#include "bsp.h"
#include "mrfi.h"
#include "nwk_types.h"
#include "nwk_api.h"
#include "bsp_leds.h"
#include "bsp_buttons.h"
#include "buffer.h"

#define SPIN_ABOUT_A_SECOND  NWK_DELAY(1000)

#define BUF_SIZE 32
#define RADIO_MESG_SIZE 10

// Information about the peer device
addr_t peerAddress = {0x13, 0x56, 0x34, 0x13};
const uint8_t localPort = 0x2d;
const uint8_t remotePort = 0x2d;

static linkID_t sLinkID = 0;

static uint8_t sRxCallback(linkID_t);

static uint8_t mesgBuffer[RADIO_MESG_SIZE] = {0};

/*static uint8_t sendBuffer[BUF_SIZE];
static uint8_t sendProducerIndex = 0;
static uint8_t sendConsumerIndex = 0;

static uint8_t receiveBuffer[BUF_SIZE];
static uint8_t receiveProducerIndex = 0;
static uint8_t receiveConsumerIndex = 0;

static uint8_t incrementIndex(uint8_t index) {
  if (++index == BUF_SIZE) {
    index = 0;
  }
  return index;
}

static uint8_t isFull(uint8_t consumerIndex, uint8_t producerIndex) {
  return consumerIndex == incrementIndex(producerIndex);
}

static uint8_t isEmpty(uint8_t consumerIndex, uint8_t producerIndex) {
  return producerIndex == consumerIndex;

}*/

Buffer serialIn = {0, 0};
Buffer serialOut = {0, 0};

void toggleLED(uint8_t which)
{
  if (1 == which)
  {
    BSP_TOGGLE_LED1();
  }
  else if (2 == which)
  {
    BSP_TOGGLE_LED2();
  }
  return;
}

void main(void) {
	// Perform network initialization
	BSP_Init();

	SMPL_Init(sRxCallback);

	// Connect to the peer device using the above info
	SMPL_Commission(&peerAddress, localPort, remotePort, &sLinkID);

	SMPL_Ioctl( IOCTL_OBJ_RADIO, IOCTL_ACT_RADIO_RXON, 0);

	// All of the below settings are to set up UART communications
	P3SEL |= BIT4 + BIT5; // Select the RX and TX bits for UART
	UCA0CTL0 = 0; // Defaults: no parity, lsb first, 8 bit data, 1 stop bit, UART, async
	UCA0CTL1 = UCSSEL_2 + UCSWRST; // Hold in reset state
	UCA0BR0 = 52; // Baud rate settings set for an 8 MHz clock
	UCA0BR1 = 0; // Baud rate settings
	UCA0MCTL = UCBRF_1 + UCBRS_0 + UCOS16; // Modulation control setting and oversampling
	UCA0CTL1 &= ~UCSWRST; // release reset
	IE2 |= UCA0RXIE; // enable rx interrupts

	while (1) {

		_bic_SR_register(GIE); // disable all interrupts

		while (!bufferedSize(&serialIn)) {
			_bis_SR_register(GIE + CPUOFF); // Enter low power mode and wait for a new message
			_bic_SR_register(GIE); // disable all interrupts
		}

		// Toggle an LED once a UART message is received and send the message back
		toggleLED(1);

		int size = 0;
		while (size = bufferedSize(&serialIn)) {
			int len = (size < RADIO_MESG_SIZE) ? size : RADIO_MESG_SIZE;
			int i = 0;
			for (; i < len; i++) {
				mesgBuffer[i] = bufferRead(&serialIn, i);
			}
			bufferConsume(&serialIn, len);

			_bis_SR_register(GIE);

			SMPL_Send(sLinkID, mesgBuffer, len);

			_bic_SR_register(GIE);
		}
	}
}

#pragma vector = USCIAB0TX_VECTOR
__interrupt void USCIAB0TX_ISR (void) {
  // Send whatever is in the send buffer over UART

	if (bufferedSize(&serialOut)) {
		UCA0TXBUF = bufferRead(&serialOut, 0);
		bufferConsume(&serialOut, 1);
	} else {
		// Disable TX interrupt when the send buffer is empty
		IE2 &= ~UCA0TXIE;
		// Might need to reset this interrupt since we didn't write any data
	}
}

#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCIAB0RX_ISR (void) {
  // Store whatever was received form UART in the receive buffer

  char val = UCA0RXBUF;
  bufferTryWrite(&serialIn, val);
  // Wake up the device
  LPM0_EXIT;
}

/* handle received frames. */
static uint8_t sRxCallback(linkID_t port)
{
	// Toggle an LED to show a message was received
	uint8_t msg[RADIO_MESG_SIZE] = {0};
	uint8_t i = 0;
	uint8_t len = 0;
	toggleLED(2);
	/* is the callback for the link ID we want to handle? */
	if (port == sLinkID)
	{
		/* Copy the data into the receive buffer */
		if ((SMPL_SUCCESS == SMPL_Receive(sLinkID, msg, &len)) && len)
		{

			for (; i < len; i++) {
				bufferTryWrite(&serialOut, msg[i]);
			}

			IE2 |= UCA0TXIE; // Enable TX interrupt to start sending the message through the uart
		}
	}
	return 1;
}
