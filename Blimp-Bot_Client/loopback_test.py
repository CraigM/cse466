import threading
import time

from interface import BlimpConnection, Command, CommandDef


class LoopbackConnection(object):
    """
    Sends spoofed packets to a BlimpConnection.

    """

    def __init__(self):
        self._write_lock = threading.Lock()
        self._message = None

    def read(self, size):
        message = None
        while True:
            with self._write_lock:
                message = self._message            
                self._message = None
            if message:
                return message
            else:
                time.sleep(1)

    def write(self, packet):
        while True:
            with self._write_lock:
                if self._message == None:
                    self._message = packet
                    return
            time.sleep(1)


def spoofed_callback(command):
    print "Received command:", command.payload


if __name__ == '__main__':
    connection = LoopbackConnection()
    blimp = BlimpConnection(spoofed_callback, connection)
    cmd_def = CommandDef.get_command_name("SENSOR_RESULT")
    print "Enter commands to echo"
    while True:
        string_to_send = raw_input()

        # Let's write some LISP!
        string_to_send = string_to_send + ("0" * (cmd_def.payload_size
            - (len(string_to_send) % cmd_def.payload_size)))
        print "Sending message of length", len(string_to_send)
        while string_to_send:
            blimp.send_command(Command(cmd_def, string_to_send[:cmd_def.payload_size]))
            string_to_send = string_to_send[cmd_def.payload_size:]
        blimp.flush()
