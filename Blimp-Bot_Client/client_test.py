import threading
import time

from client import Application
from interface import Command, CommandDef, BlimpConnection


class ConnectionSpoof(object):
    """
    Spoofs a connection that automatically responds to ping requests.

    """

    def __init__(self):
        self._auto_ping = True
        self._lock = threading.Lock()
        self._system_data = "A"
        self._callback = None

    def send_command(self, cmd):
        command_def = cmd.command_def
        returned_command = None

        with self._lock:
            if command_def is CommandDef.get_command_name("RUN"):
                self._auto_ping = True
            elif command_def is CommandDef.get_command_name("STOP"):
                self._auto_ping = False
            elif command_def is CommandDef.get_command_name("PING"):
                if self._auto_ping:
                    returned_command = Command(CommandDef.get_command_name(
                        "PING_RESPONSE",
                    ))
            elif command_def is CommandDef.get_command_name("QUERY_STATUS"):
                returned_command = Command(
                    CommandDef.get_command_name("STATUS_RESPONSE"),
                    self._system_data,
                )
        if returned_command and self._callback:
            return self._callback(returned_command)

    def set_callback(self, cmd):
        self._callback = cmd

    def flush(self):
        pass
    

if __name__ == "__main__":
    connection = ConnectionSpoof()
    app = Application(connection)
    connection.app = app
    app.mainloop()
