from Tkinter import *
import argparse
import collections
import io
import struct
import sys
import threading
import time

import PIL.Image
import ImageTk
import serial

from interface import BlimpConnection, Command, CommandDef, get_sensor_result

# String literals.
RUN = "Run"
STOP = "Stop"
CONNECTED = "Connected"
NOT_CONNECTED = "Not connected"
FORWARD = "Forward"
REVERSE = "Reverse"
RIGHT = "Right"
LEFT = "Left"
DOWN = "Down"
ALT = "Altitude"
SEND = "Send"
SET = "Set PID values"

# Color literals.
WHITE = "#ffffff"
DARK_GRAY = "#dddddd"
BLUE = "#0000ff"
RED = "#B20000"
GREEN = "#228b22"

# Display data.
DELTA_THETA = 10
DELTA_ACCEL = 10
DELTA_ALT = 10
REDRAW_MS = 30

# How long we can go without a ping before considering the blimp disconnected.
PING_TIMEOUT = 5
# How frequently to send a ping (in seconds).
PING_FREQ = 0.05

# Flag information.
SENSOR_COMMANDS = collections.deque(
    Command(CommandDef.get_command_name("POLL_SENSOR"), chr(x)) for x
        in range(5)
)

# Autopilot information.
MANUAL_OVERRIDE_OPTIONS = (
    ("Enable manual vertical control.", False),
    ("Enable manual horizontal control.", True),
)
SET_POINT = "Set point"
PROPORTIONAL = "Proportional"
INTEGRAL = "Integral"
DERIVATIVE = "Derivative"
VERTICAL_CONTROL_FIELDS = (
    SET_POINT,
    PROPORTIONAL,
    INTEGRAL,
    DERIVATIVE,
)


class Row(object):
    """
    Used for specifying the row of items.

    """

    def __init__(self, initial=0):
        self._val = initial - 1

    def get(self):
        return self._val

    def next(self):
        """
        Pre-increment and return.

        """
        self._val += 1
        return self.get()


class Motor(object):
    """
    Representation of a motor.

    """

    def __init__(self, delta, return_delta, max):
        self._value = 0
        self._delta = delta
        self._return_delta = return_delta
        self._max = max
        self._triggered = False

    def increment(self):
        self._value += self._delta
        self._triggered = True

    def decrement(self):
        self._value -= self._delta
        self._triggered = True

    def get(self):
        if self._value > self._max:
            self._value = self._max
        if self._value < -self._max:
            self._value = -self._max
        return self._value

    def age(self):
        if not self._triggered:
            if abs(self._value) < self._return_delta:
                self._value = 0
            if self._value > 0:
                self._value = self._value - self._return_delta
            elif self._value < 0:
                self._value = self._value + self._return_delta
        self._triggered = False

    def age_and_get(self):
        self.age()
        return self.get()


class Option(object):
    """
    Represents a single option as a check box.

    """

    def __init__(self, name, default=False):
        self.name = name
        self.int_var = IntVar()
        if default:
            self.int_var.set(1)


class Mask(object):
    """
    Represents a series of checkboxes that can be used to set up a bitmask.

    """

    def __init__(self, options):
        """
        options - a list of string values corresponding to the mask values in
        increasing bit order.

        """
        self.options = tuple(Option(name, default) for name, default in options)

    def get_mask(self):
        """
        Returns the mask as an integer.

        """
        mask = 0
        for i, option in enumerate(self.options):
            mask |= (option.int_var.get() << i)
        return mask
        

class Application(Frame):
    """
    The top-level application, containing a left panel for movement controls and
    a right panel for system commands.

    """

    def __init__(self, connection, log_file):
        Frame.__init__(self, None,
            bg = WHITE,
        )
        self._log_file = log_file
        if self._log_file:
            with open(self._log_file, "w") as f:
                f.write("Time\tSetpoint\tForward\tReverse\tRight\tLeft\tDown")
        self._blimp_bot_connection = connection
        self.grid()
        self._init_movement_panel()
        self._init_system_panel()
        self.master['bg'] = WHITE
        self._theta = Motor(DELTA_THETA, 5, 45)
        self._accel = Motor(DELTA_ACCEL, 5, 45)
        self._alt = Motor(DELTA_ALT, 5, 45)
        self.master.bind("w", self.forward)
        self.master.bind("a", self.rotate_left)
        self.master.bind("s", self.reverse)
        self.master.bind("d", self.rotate_right)
        self.master.bind("c", self.lower)
        self.master.bind("v", self.higher)
        self.master.title("CSE 466: Blimp Bot")
        self._redraw_lock = threading.Lock()
        self.master.after(REDRAW_MS, self._redraw)
        self._last_ping = 0
        self._running = False
        self._pid_values = None
        ping_thread = threading.Thread(target=self._ping)
        ping_thread.daemon = True
        ping_thread.start()
        # This needs to go at the end.
        self._blimp_bot_connection.set_callback(self._callback)

    def _init_movement_panel(self):
        """
        Creates the panel on the left that controls movement.

        """
        self._movement_panel = Frame(self,
            bg = WHITE,
            padx = 10,
            pady = 10,
        )
        self._movement_panel.grid()
        self.blimp_bitmap = PIL.Image.open("res/blimp.png")
        blimp_image = ImageTk.PhotoImage(self.blimp_bitmap)
        self._image_label = Label(self._movement_panel, image=blimp_image,
            bg = WHITE,
            height = 400,
            takefocus = True,
            width = 600,
        )
        self._image_label.image = blimp_image # To avoid getting GCed
        self._image_label.grid(columnspan=2)
        
        self._left_label = Label(self._movement_panel,
            anchor = "w",
            bg = WHITE,
            font = ("Helvetica", "16"),
            text = LEFT,
        )
        self._left_label.grid(row=1, padx=10, pady=10)

        bar_image = ImageTk.PhotoImage(PIL.Image.open("res/bar.png"))
        self._left_bar = Label(self._movement_panel, image=bar_image,
            bg = WHITE,
            pady=10,
        )
        self._left_bar.image = bar_image
        self._left_bar.grid(column=1, row=1, padx=10, pady=10)

        indicator_image = ImageTk.PhotoImage(PIL.Image.open("res/indicator.png"))
        self._left_indicator = Label(self._left_bar, image=indicator_image,
            bd = 0,
        )
        self._left_indicator.image = indicator_image
        self._left_indicator.place(anchor="center", relx=0.5, rely=0.5)
        
        self._right_label = Label(self._movement_panel,
            anchor = "w",
            bg = WHITE,
            font = ("Helvetica", "16"),
            text = RIGHT,
        )
        self._right_label.grid(row=2, padx=10, pady=10)

        self._right_bar = Label(self._movement_panel, image=bar_image,
            bg = WHITE,
            pady=10,
        )
        self._right_bar.image = bar_image
        self._right_bar.grid(column=1, row=2, padx=10, pady=10)
        
        self._right_indicator = Label(self._right_bar, image=indicator_image,
            bd = 0,
        )
        self._right_indicator.image = indicator_image
        self._right_indicator.place(anchor="center", relx=0.5, rely=0.5)
        
        self._alt_label = Label(self._movement_panel,
            anchor = "w",
            bg = WHITE,
            font = ("Helvetica", "16"),
            text = ALT,
        )
        self._alt_label.grid(row=3, padx=10, pady=10)

        self._alt_bar = Label(self._movement_panel, image=bar_image,
            bg = WHITE,
            pady=10,
        )
        self._alt_bar.image = bar_image
        self._alt_bar.grid(column=1, row=3, padx=10, pady=10)
        
        self._alt_indicator = Label(self._alt_bar, image=indicator_image,
            bd = 0,
        )
        self._alt_indicator.image = indicator_image
        self._alt_indicator.place(anchor="center", relx=0.5, rely=0.5)

    def _init_system_panel(self):
        """
        Initializes the system panel full of buttons on the right.

        """
        next_row = Row()
        self._system_panel = Frame(self,
            bg = WHITE,
            padx=10,
        )
        self._system_panel.grid(column=1, row=next_row.next(), padx=10, pady=10, sticky=N+S)

        self._run_text = StringVar()
        self._run_text.set(RUN)
        self._run_button = Button(self._system_panel,
            activebackground = BLUE,
            activeforeground = WHITE,
            anchor = "center",
            bd = 0,
            bg = BLUE,
            command = self._run,
            fg = WHITE,
            font = ("Helvetica", "24", "bold"),
            padx = 5,
            pady = 5,
            relief = "flat",
            textvariable = self._run_text,
            width = 5,
        )
        self._run_button.grid(pady=10, sticky=W)

        self._connected_text = StringVar()
        self._connected_text.set(NOT_CONNECTED)
        self._connected_label = Label(self._system_panel,
            anchor = "center",
            bg = RED,
            fg = WHITE,
            font = ("Helvetica", "16"),
            padx = 5,
            pady = 5,
            textvariable = self._connected_text,
            width = 15,
        )
        self._connected_label.grid(column=1, row=next_row.get())

        self._init_status_pane(next_row)

        self._packing = []
        self._packing.append(Frame(self._system_panel,
            bg = WHITE,
        ))
        self._packing[-1].grid(column=0, row=next_row.next(), pady=5, columnspan=2)

        self._init_autopilot_pane(next_row)

        self._packing.append(Frame(self._system_panel,
            bg = WHITE,
        ))
        self._packing[-1].grid(column=0, row=next_row.next(), pady=5, columnspan=2)

        self._init_sensor_pane(next_row)

    def _init_status_pane(self, next_row):
        """
        Initializes the status pane.

        """
        self._status_title = Label(self._system_panel,
            bg = DARK_GRAY,
            font = ("Helvetica", "16", "bold"),
            padx = 5,
            pady = 5,
            text = "Status",
        )
        self._status_title.grid(column=0, row=next_row.next(), sticky=W)
        self._status_field_text = StringVar()
        self._status_field = Label(self._system_panel,
            anchor = "w",
            bg = DARK_GRAY,
            font = ("Helvetica", "12"),
            padx = 5,
            pady = 5,
            textvariable = self._status_field_text,
        )
        self._status_field.grid(column=0, row=next_row.next(), columnspan=2, sticky=E+W)

    def _init_autopilot_pane(self, next_row):
        """
        Initializes the autopilot pane.

        """
        self._autopilot_title = Label(self._system_panel,
            bg = DARK_GRAY,
            font = ("Helvetica", "16", "bold"),
            padx = 5,
            pady = 5,
            text = "Autopilot Parameters",
        )
        self._autopilot_title.grid(column=0, row=next_row.next(), sticky=W)
        self._autopilot_frame = Frame(self._system_panel,
            bg = DARK_GRAY,
            padx = 5,
            pady = 5,
        )
        self._autopilot_frame.grid(column=0, row=next_row.next(), columnspan=2, sticky=E+W)
        next_row = Row()
        
        self._autopilot_mask = Mask(MANUAL_OVERRIDE_OPTIONS)
        for option in self._autopilot_mask.options:
            c = Checkbutton(self._autopilot_frame,
                anchor = W,
                bg = DARK_GRAY,
                font = ("Helvetica", "12"),
                padx = 5,
                pady = 5,
                text = option.name,
                variable = option.int_var,
            )
            c.grid(column=0, row=next_row.next(), columnspan=2, sticky=E+W)
            option.checkbutton = c

        self._vertical_control_fields = {}
        for field in VERTICAL_CONTROL_FIELDS:
            Label(self._autopilot_frame,
                anchor = W,
                bg = DARK_GRAY,
                font = ("Helvetica", "12"),
                padx = 5,
                pady = 5,
                text = field,
            ).grid(column=0, row=next_row.next(), sticky=E+W)
            self._vertical_control_fields[field] = Entry(self._autopilot_frame,
                font = ("Helvetica", "10"),
            )
            self._vertical_control_fields[field].grid(
                column=1,
                row=next_row.get(),
                sticky=E+W
            )
        self._set_vertical_control_fields_button = Button(self._autopilot_frame,
            activebackground = BLUE,
            activeforeground = WHITE,
            anchor = "center",
            bd = 0,
            bg = BLUE,
            command = self._set_vertical_control,
            fg = WHITE,
            font = ("Helvetica", "12", "bold"),
            text = SET,
        )
        self._set_vertical_control_fields_button.grid(
            column=1,
            row=next_row.next(),
        )

    def _init_sensor_pane(self, next_row):
        """
        Initializes the sensor results pane.

        """
        self._sensor_title = Label(self._system_panel,
            bg = DARK_GRAY,
            font = ("Helvetica", "16", "bold"),
            padx = 5,
            pady = 5,
            text = "Sensor Information",
        )
        self._sensor_title.grid(column=0, row=next_row.next(), sticky=W)
        self._sensor_field_text = StringVar()
        self._sensor_field = Label(self._system_panel,
            anchor = "w",
            bg = DARK_GRAY,
            font = ("Helvetica", "12"),
            height = 5,
            justify = "left",
            padx = 5,
            pady = 5,
            textvariable = self._sensor_field_text,
        )
        self._sensor_field.grid(column=0, row=next_row.next(), columnspan=2, sticky=E+W)

    def _run(self):
        """
        Callback invoked when the run button is clicked.

        """
        if self._running:
            self._run_text.set(RUN)
            command = Command(CommandDef.get_command_name("STOP"))
            self._blimp_bot_connection.send_command(command)
            self._running = False
        else:
            self._run_text.set(STOP)
            command = Command(CommandDef.get_command_name("RUN"))
            self._blimp_bot_connection.send_command(command)
            self._running = True
        self._blimp_bot_connection.flush()

    def forward(self, event):
        """
        Callback invoked when the blimp should move forward.

        """
        with self._redraw_lock:
            self._accel.increment()

    def reverse(self, event):
        """
        Callback invoked when the blimp should move backwards.

        """
        with self._redraw_lock:
            self._accel.decrement()

    def rotate_left(self, event):
        """
        Callback invoked when the blimp should rotate left.

        """
        with self._redraw_lock:
            self._theta.increment()

    def rotate_right(self, event):
        """
        Callback invoked when the blimp should rotate right.

        """
        with self._redraw_lock:
            self._theta.decrement()

    def higher(self, event):
        """
        Callback invoked when the blimp should increase altitude.
        
        """
        with self._redraw_lock:
            self._alt.increment()

    def lower(self, event):
        """
        Callback invoked when the blimp should decrease altitude.

        """
        with self._redraw_lock:
            self._alt.decrement()

    def _set_vertical_control(self):
        """
        Callback invoked when the user wants to set the vertical control fields.

        """
        with self._redraw_lock:
            tmp = {}
            for field in VERTICAL_CONTROL_FIELDS:
                try:
                    tmp[field] = int(self._vertical_control_fields[field].get())
                except ValueError:
                    return
            self._pid_values = tmp

    def _redraw(self):
        """
        Redraws the blimp with the given rotation.

        """
        with self._redraw_lock:
            theta = self._theta.age_and_get()
            accel = self._accel.age_and_get()
            alt = self._alt.age_and_get()

        # Set up our rotated blimp image.
        blimp_image = ImageTk.PhotoImage(self.blimp_bitmap.rotate(
            theta,
            PIL.Image.BICUBIC,
            True,
        ))
        self._image_label['image'] = blimp_image
        self._image_label.image = blimp_image # To avoid getting GCed

        # Set up our indicator bars.
        delta_theta = theta / 200.0
        delta_acc = accel / 200.0
        delta_alt = alt / 100.0

        self._left_indicator.place_forget()
        self._left_indicator.place(
            anchor = "center",
            in_ = self._left_bar,
            relx = 0.5 - delta_theta + delta_acc,
            rely = 0.5,
        )
        self._right_indicator.place_forget()
        self._right_indicator.place(
            anchor = "center",
            in_ = self._right_bar,
            relx = 0.5 + delta_theta + delta_acc,
            rely = 0.5
        )
        self._alt_indicator.place_forget()
        self._alt_indicator.place(
            anchor = "center",
            in_ = self._alt_bar,
            relx = 0.5 + delta_alt,
            rely = 0.5,
        )

        if self._last_ping + PING_TIMEOUT < time.time():
            # We have a ping timeout.
            self._connected_text.set(NOT_CONNECTED)
            self._connected_label['bg'] = RED
        else:
            # Ping is good, healthy connection.
            if self._connected_text.get() == NOT_CONNECTED:
                # Beep when we switch from not connected to connected.
                print '\007'
            self._connected_text.set(CONNECTED)
            self._connected_label['bg'] = GREEN

        self.master.after(REDRAW_MS, self._redraw)

    def _callback(self, command):
        """
        Callback called after a command is received from the blimp.

        """
        command_def = command.command_def
        self._last_ping = time.time()
        if command_def is CommandDef.get_command_name("STATUS_RESPONSE"):
            self._status_field_text.set("Status: {s}".format(s=command.payload))
        if command_def is CommandDef.get_command_name("SENSOR_RESULT"):
            forward, reverse, right, left, down = get_sensor_result(command.payload)
            self._sensor_field_text.set("Forward: {forward}\n"
                "Reverse: {reverse}\n"
                "Right: {right}\n"
                "Left: {left}\n"
                "Down: {down}".format(
                    forward = forward,
                    reverse = reverse,
                    right = right,
                    left = left,
                    down = down,
                )
            )
            if self._log_file:
                with self._redraw_lock:
                    if self._pid_values:
                        set_point = self._pid_values[SET_POINT]
                    else:
                        set_point = -1
                with open(self._log_file, "a") as f:
                    f.write("\r\n{time}\t{set}\t{forward}\t{rev}\t{right}\t{left}\t{down}".format(
                        time = time.time(),
                        set = set_point,
                        forward = forward,
                        rev = reverse,
                        right = right,
                        left = left,
                        down = down
                    ))

    def _ping(self):
        """
        Sends a ping command periodically.

        """
        run = Command(CommandDef.get_command_name("RUN"))
        stop = Command(CommandDef.get_command_name("STOP"))
        ping = Command(CommandDef.get_command_name("PING"))
        query_status = Command(CommandDef.get_command_name("QUERY_STATUS"))
        motor_def = CommandDef.get_command_name("LL_SET")
        manual_mask = CommandDef.get_command_name("MANUAL_CONTROL_MASK")
        tune = CommandDef.get_command_name("TUNE")
        while True:
            if self._running:
                self._blimp_bot_connection.send_command(run)
                with self._redraw_lock:
                    theta = self._theta.get()
                    accel = self._accel.get()
                    alt = self._alt.get()
                    pid_values = self._pid_values

                left_motor = (accel - theta) * 2
                right_motor = (theta + accel) * 2
                alt_motor = alt * 4
                self._blimp_bot_connection.send_command(Command(
                    motor_def,
                    chr(1) + (chr(1) if left_motor > 0 else chr(0)) + chr(abs(left_motor))
                ))
                self._blimp_bot_connection.send_command(Command(
                    motor_def,
                    chr(2) + (chr(1) if right_motor > 0 else chr(0)) + chr(abs(right_motor))
                ))
                self._blimp_bot_connection.send_command(Command(
                    motor_def,
                    chr(0) + (chr(1) if alt_motor > 0 else chr(0)) + chr(abs(alt_motor))
                ))
                if pid_values is not None:
                    self._blimp_bot_connection.send_command(Command(
                        tune,
                        struct.pack(
                            "<hhhh",
                            *[pid_values[field] for field in VERTICAL_CONTROL_FIELDS]
                        )
                    ))
            else:
                self._blimp_bot_connection.send_command(stop)
            #SENSOR_COMMANDS.rotate()
            #self._blimp_bot_connection.send_command(SENSOR_COMMANDS[0])
            #self._blimp_bot_connection.send_command(query_status)
            self._blimp_bot_connection.send_command(Command(
                manual_mask,
                chr(self._autopilot_mask.get_mask()),
            ))
            self._blimp_bot_connection.flush()
            time.sleep(PING_FREQ)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", dest="log", help="Write sensor data to the given log file.")
    parser.add_argument("-c", dest="connection", help="The connection to use.", required=True)
    args = parser.parse_args()
    ser = serial.serial_for_url(args.connection)
    blimp_connection = BlimpConnection(None, ser)
    app = Application(blimp_connection, args.log)
    app.mainloop()
