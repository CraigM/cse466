import collections
import io
import string
import struct
import threading


DEBUG = True
PRINT_READ_BUFFER = False
PRINT_WRITE_BUFFER = False
PRINT_INVALID_PACKETS = True

# This is the size with flag bytes.
PACKET_SIZE = 16
READ_SIZE = 16
DATA_LENGTH = 12
FLAG = 'U'


class CommandDef(object):
    """
    Definition of a command either to or from the blimp.

    """

    @classmethod
    def get_command_byte(cls, byte):
        """
        Gets the command with the specified signal byte, or None if it's not
        found.

        """
        return cls._commands_byte.get(byte)

    @classmethod
    def get_command_name(cls, name):
        """
        Gets the command with the specified name.

        """
        return cls._commands_name.get(name)

    def __init__(self, byte, payload_size=None):
        self.byte = byte
        self.payload_size = payload_size

CommandDef._commands_name = {

    # RUN/STOP controller signals
    "RUN": CommandDef(chr(1)),
    "STOP": CommandDef(chr(2)),

    # Low-level motor control commands
    "LL_SET": CommandDef(chr(11), 3),

    # High-level motion control
    "HL_SET": CommandDef(chr(21), 1),
    "TUNE": CommandDef(chr(22), 8),
    "QUERY_MOTION": CommandDef(chr(23)),

    # Sensing
    "POLL_SENSOR": CommandDef(chr(31), 1),

    # System Monitor
    "PING": CommandDef(chr(41)),
    "QUERY_STATUS": CommandDef(chr(42)),
    "RESET": CommandDef(chr(43)),

    # Responses
    "MOTION_RESPONSE": CommandDef(chr(51), 1),
    "SENSOR_RESULT": CommandDef(chr(61), 10),
    "PING_RESPONSE": CommandDef(chr(71)),
    "STATUS_RESPONSE": CommandDef(chr(72), 1),

    # Autopilot
    "MANUAL_CONTROL_MASK": CommandDef(chr(81), 1),
}

CommandDef._commands_byte = dict([
    (y.byte, y) for x, y in CommandDef._commands_name.iteritems()
])


class Command(object):
    """
    An actual instance of a command.

    """

    def __init__(self, cmd_def, payload=""):
        self.command_def = cmd_def
        self.payload = payload

    def __len__(self):
        return 1 + len(self.payload)

    def __str__(self):
        return "{byte}:{payload}".format(
            byte = ord(self.command_def.byte),
            payload = ",".join(str(ord(x)) for x in self.payload),
        )

    def __repr__(self):
        return str(self)

    def to_bytes(self):
        """
        Returns this command in the format it will be sent over the wire in.

        """
        return self.command_def.byte + self.payload


class BlimpConnection(object):
    """
    Represents a connection to a blimp-bot. Allows for the sending and
    receiving of commands.

    Please note that write and flush are NOT thread-safe, and some form of
    external locking will be necessary.

    """

    def __init__(self, callback, connection):
        self._connection = connection
        self._callback = callback
        self._outgoing_commands = collections.deque()
        self._write_lock = threading.Lock()
        thread = threading.Thread(target=self._read_loop)
        thread.daemon = True
        thread.start()

    def set_callback(self, callback):
        """
        Sets the current callback.

        """
        with self._write_lock:
            self._callback = callback

    def _read_loop(self):
        """
        Constantly reads the serial connection and calls callback with any data.

        """
        buf = ""
        packet_stream = ""
        while True:
            try:
                buf += self._connection.read(READ_SIZE)
                if PRINT_READ_BUFFER: print "Read buffer contents:", ords(buf)

                packet, buf = self._get_packet(buf)
                if not packet:
                    continue
                if DEBUG: print "Read packet", ords(packet)
                
                calculated_crc = crc(packet[:DATA_LENGTH])
                given_crc = bytes_to_int(packet[DATA_LENGTH:])

                if calculated_crc != given_crc:
                    # Incorrect crc means data corruption.
                    # Remember to add 2 for the flag bytes.
                    if PRINT_INVALID_PACKETS:
                        print "Calculated CRC", calculated_crc, "does not match given CRC", given_crc
                    buf = buf[len(packet) + 2:]
                    continue
                packet_stream += packet[:-2]
                if DEBUG: print "packet_stream is now", ords(packet_stream)

                while len(packet_stream) > 0:
                    # Now that we've added to the stream, find all of the
                    # commands in it.
                    command_byte = packet_stream[0]
                    command_def = CommandDef.get_command_byte(command_byte)
                    
                    if command_def is None:
                        # This is a bad byte.
                        if PRINT_INVALID_PACKETS: print "Bad byte", ords(command_byte), "in packet_stream."
                        packet_stream = packet_stream[1:]
                        continue 

                    to_pop = 1
                    payload = ""
                    if command_def.payload_size:
                        if command_def.payload_size >= len(packet_stream):
                            # We haven't finished reading this command yet.
                            # Don't touch anything, just wait for the rest.
                            if DEBUG: print "Haven't finished reading this command yet."
                            break
                        payload = packet_stream[1:command_def.payload_size+1]
                        to_pop += command_def.payload_size

                    if DEBUG: print "Popping first", to_pop, "items from item string."
                    packet_stream = packet_stream[to_pop:]

                    if DEBUG: print "Calling callback."
                    if self._callback:
                        self._callback(Command(command_def, payload))

            except ValueError:
                # We're still reading a broken packet, so just clear the buffer.
                buf = ""

    def _get_packet(self, buf):
        """
        Parses the buffer and returns the first valid packet and the new buffer
        as a tuple.

        """
        if len(buf) < PACKET_SIZE:
            return (None, buf)

        for i in range(len(buf) - PACKET_SIZE + 1):
            # Walk the buffer, attempting to return a sequence PACKET_SIZE bytes
            # long that starts and ends with flag bytes.
            if buf[i] == buf[i+PACKET_SIZE-1] == FLAG:
                # We have a packet! Make sure to chop the flag bytes off when
                # we return.
                packet = buf[i+1:i+PACKET_SIZE-1]
                return (packet, buf[i+PACKET_SIZE:])

        if len(buf) > PACKET_SIZE * 2:
            return (None, buf[-(PACKET_SIZE*2):])
        return (None, buf[buf.index(FLAG):])

    def send_command(self, cmd):
        """
        Sends the given command to the blimp. Propagates IOExceptions upwards.

        """
        with self._write_lock:
            self._outgoing_commands.append(cmd)
            write_bytes = ""
            while sum(len(x) for x in self._outgoing_commands) >= DATA_LENGTH:
                # Create a packet, ensuring that no command spans packet boundaries.
                while self._outgoing_commands and (len(write_bytes) +
                        len(self._outgoing_commands[0]) <= DATA_LENGTH):
                    write_bytes += self._outgoing_commands[0].to_bytes()
                    self._outgoing_commands.popleft()

                # Now pad our data with ping requests.
                write_bytes += (CommandDef.get_command_name("PING").byte *
                    (DATA_LENGTH - len(write_bytes)))

                # Add the flags and CRC and we're done!
                packet = FLAG + write_bytes + int_to_bytes(crc(write_bytes)) + FLAG
                if DEBUG: print "Sending packet", ords(packet)
                assert len(packet) == PACKET_SIZE
                self._connection.write(packet)
            if PRINT_WRITE_BUFFER: print "Write buffer contents:", self._outgoing_commands

    def flush(self):
        """
        Flushes the output buffer, padding with ping requests in order to hit
        a full packet size.

        """
        ping = Command(CommandDef.get_command_name("PING"))
        for i in range(DATA_LENGTH - (len(self._outgoing_commands) % DATA_LENGTH)):
            self.send_command(ping)


def crc(crc_str):
    """
    Calculate a CRC.

    """
    old_crc = 0
    for byte in crc_str:
        x = ((old_crc >> 8) ^ ord(byte)) & 0xFF
        x ^= x >> 4

        old_crc = (((old_crc << 8) ^ (x << 12) ^ (x << 5)) ^ x) & ((1 << 16) - 1)

    return old_crc

def bytes_to_int(byte_str):
    """
    Takes a byte string and converts it to an int.

    """
    total = 0
    for pos, byte in enumerate(byte_str):
        total += (256 ** pos) * ord(byte)
    return total

def int_to_bytes(num):
    """
    Takes an integer and converts it into a little-endian byte stream.

    """
    return chr(num & 255) + chr((num & (255 << 8)) >> 8)

def ords(stream):
    """
    Returns the ordinal values of each byte in a stream joined by commas.

    """
    return ",".join(str(ord(x)) for x in stream)

def get_sensor_result(payload):
    """
    Unpacks a sensor result payload into a tuple containing the forward,
    reverse, right, left, and down values.

    """
    return struct.unpack("<hhhhh", payload)
