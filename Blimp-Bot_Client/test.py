import time

from interface import BlimpConnection


MESSAGES_LIST = [
    "# Test starting halfway through.",
    "llo my n=\x94\xc7U",
    "Uame is Gig=a+\xd7U",
    "Unto! Glad=to\roU",
    "Umeet youGGGG\x04\xe0U",

    "# Test a long sequence of valid messages.",
    "U=hello my n=\x94\xc7U",
    "Uame is Gig=a+\xd7U",
    "Unto! Glad=to\roU",
    "Umeet youGGGG\x04\xe0U",
    "U=hello my n=\x94\xc7U",
    "Uame is Gig=a+\xd7U",
    "Unto! Glad=to\roU",
    "Umeet youGGGG\x04\xe0U",

    "# Test a split packet.",
    "U=hello my n=\x94\xc7U",
    "Uame is",
    " Gig=a+\xd7U",
    "Unto! Glad=to\roU",
    "Umeet youGGGG\x04\xe0U",

    "# Test an erasure.",
    "U=hello my n=\x94\xc7U",
    "Uam Gig=a+\xd7U",
    "Unto! Glad=to\roU",
    "Umeet youGGGG\x04\xe0U",

    "# Test a packet that contains a flag byte.",
    "U=hello my n=\x94\xc7U",
    "Uame is Uig=ak\x0eU",
    "Unto! Glad=to\roU",
    "Umeet youGGGG\x04\xe0U",
]


class SpoofedSender(object):
    """
    Sends spoofed packets to a BlimpConnection.

    """

    def __init__(self, messages_list):
        self.messages_list = messages_list
        self.position = 0

    def read(self, size):
        while True:
            message = self.messages_list[self.position]
            self.position = (self.position + 1) % len(self.messages_list)

            if message[0] == "#":
                # The message is a comment
                print
                print message
                continue

            if self.position == 0:
                lskajdlkjasf
            return message


def spoofed_callback(command):
    print "Received command:", command.payload


if __name__ == '__main__':
    sender = SpoofedSender(MESSAGES_LIST)
    connection = BlimpConnection(spoofed_callback, sender)
    while True:
        time.sleep(1)
