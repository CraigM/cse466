/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#include "tcb/tcb.h"
#include "os.h"
#include "tasks.h"

TaskQueue taskQueue={&tasks[0],NUM_TASKS,0};

void run(){
	int tskErr=0;
	// Simply cycle through all of the tasks, intfinitely
	while (1){
		exec_next_task(&taskQueue,&tskErr);
	}
}
