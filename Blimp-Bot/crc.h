/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#ifndef CRC_H_
#define CRC_H_

unsigned int crc(unsigned int old_crc, char data);

#endif // CRC_H_
