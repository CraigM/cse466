/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#include "msp430f5510.h"
#include "sysmonitor.h"
#include "../globals.h"

int sys_monitor(void* data) {
        // clear watchdog
        WDTCTL = WDTPW + WDTCNTCL;

        // Check state is on or off
        if (globals.isRunning) {
                P5OUT ^= BIT1;
        } else {
                P5OUT &= ~BIT1;
        }

        // Light LED while connected
        if (globals.isConnected) {
                PJOUT ^= BIT3;
        } else {
                PJOUT &= ~BIT3;
        }

        
        return 0;
}
