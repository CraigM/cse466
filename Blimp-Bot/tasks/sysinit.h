/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#ifndef SYSINIT_H_
#define SYSINIT_H_

// Function to initialize the system
//		* Clock set to 12 MHz
//		* Timer module initialized
//		* USCI initialized
//		* ADC initialized
//		* Initializes GPIO ports
//		* Sensor and motor hardware put into safe state
//		* Configures the watchdog timer
int sys_init (void*);

#endif // TCB_H_
