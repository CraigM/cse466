/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#ifndef SENSOR_H_
#define SENSOR_H_

int sensor_task (void*);

#endif
