/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#ifndef MOTORCONTOLLER_H_
#define MOTORCONTOLLER_H_

int motor_task (void*);

#endif
