/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#include "motioncontroller.h"
#include "../globals.h"
#include "../pid.h"

PIDSettings piddisconnectedVertSettings = { -8, -2, -512};
PIDSettings pidForwardSettings = { -2, 0, 0 };
PIDSettings pidTurnSettings = { -6, 0, 0 };
PID pidVert = { &piddisconnectedVertSettings, 0, 0 };
PID pidForward = { &pidForwardSettings, 0, 0 };
PID pidTurn = { &pidTurnSettings, 0, 0 };

#define DRIVE_DEVISIOR 4

char makeDir(int *drive, unsigned char limit) {
	char direction;
	if (*drive > 0) {
		direction = FORWARD;
	} else {
		direction = REVERSE;
		*drive = -*drive;
	}
	if (*drive > limit) {
		*drive = limit;
	}
	return direction;
}

int motion_task(void *input) {
	// Do AI here

	// Fall back to fail-safe settings if disconnected
	Autopilot *autopilot =
			globals.isConnected ?
					&globals.connectedAutopilot :
					&globals.disconnectedAutopilot;

	pidVert.settings=globals.isConnected ?
			&globals.pidVertSettings :
			&piddisconnectedVertSettings;

	static const int forwardSetPoint = -200;
	static const int turnThreshold = 50;

	if (globals.newSensorResult) {
		globals.newSensorResult = 0;
		if (globals.sensingDirection == RANGE_DOWN) {

			volatile int vertRange = globals.smoothedRange[RANGE_DOWN];
			volatile int forwardVal = globals.smoothedRange[RANGE_FORWARD]
					- globals.smoothedRange[RANGE_REVERSE];
			volatile int rightTurnVal = globals.smoothedRange[RANGE_RIGHT]
					- globals.smoothedRange[RANGE_LEFT];

			if (rightTurnVal>0) {
				rightTurnVal=rightTurnVal-turnThreshold;
			}
			if (rightTurnVal<0) {
				rightTurnVal=rightTurnVal+turnThreshold;
				if (rightTurnVal>0) {
					rightTurnVal=0;
				}
			}

			int vertDrive = updatePID(&pidVert, autopilot->verticleSetPoint, vertRange);
			int forwardDrive = updatePID(&pidForward, forwardVal, forwardSetPoint);
			int rightTurnDrive = updatePID(&pidTurn, 0, rightTurnVal);

			vertDrive = vertDrive / DRIVE_DEVISIOR;
			if (vertDrive<0) {
				vertDrive = vertDrive / 16;
			}


			char direction = makeDir(&vertDrive, autopilot->power);

			if (!(globals.isConnected
					&& (globals.manualControlMask & MODE_VERTICAL))) {
				// If vertical control is not under manual control
				if ((autopilot->mode) & MODE_VERTICAL) {
					// Use vertical control
					globals.motor[VERTICAL].power = vertDrive;
					globals.motor[VERTICAL].direction = direction;
				} else {
					// vertical off
					globals.motor[VERTICAL].power = 0;
					globals.motor[VERTICAL].direction = FORWARD;
				}
			}

			int rightMotorDrive = 0;
			int leftMotorDrive = 0;
			if ((autopilot->mode) & MODE_FORWARD) {
				// Use forward control
				rightMotorDrive += forwardDrive;
				leftMotorDrive += forwardDrive;
			}

			if ((autopilot->mode) & MODE_TURN) {
				// Use vertical control
				rightMotorDrive += rightTurnDrive;
				leftMotorDrive -= rightTurnDrive;
			}

			rightMotorDrive = rightMotorDrive / DRIVE_DEVISIOR;
			leftMotorDrive = leftMotorDrive / DRIVE_DEVISIOR;
			if (rightMotorDrive>0) {
				rightMotorDrive = rightMotorDrive / 2;
			}
			if (leftMotorDrive>0) {
				leftMotorDrive = leftMotorDrive / 2;
			}
			char rightDirection = makeDir(&rightMotorDrive, autopilot->power);
			char leftDirection = makeDir(&leftMotorDrive, autopilot->power);

			if (!(globals.isConnected
					&& (globals.manualControlMask & MODE_HORIZONTAL))) {
				// If vertical control is not under manual control

				// Use HORIZONTAL control
				globals.motor[RIGHT].power = rightMotorDrive;
				globals.motor[RIGHT].direction = rightDirection;
				globals.motor[LEFT].power = leftMotorDrive;
				globals.motor[LEFT].direction = leftDirection;
			}
		}
	}
	return 0;
}
