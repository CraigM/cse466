/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

int communication_task (void*);

#endif
