/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#include "msp430f5510.h"
#include "../globals.h"
#include "sysinit.h"

void set_vcore_up(unsigned int level);

int sys_init(void *input) {
	if (globals.isInitialized) {
		return 0;
	}

	// Increase Vcore setting to level1 to support fsystem=12MHz
	// NOTE: Change core voltage one level at a time..
	set_vcore_up(0x01);

	// Initialize DCO to 12MHz
	__bis_SR_register(SCG0);
	// Disable the FLL control loop
	UCSCTL0 = 0x0000; // Set lowest possible DCOx, MODx
	UCSCTL1 = DCORSEL_5; // Select DCO range 24MHz operation
	UCSCTL2 = FLLD_1 + 374; // Set DCO Multiplier for 12MHz
							// (N + 1) * FLLRef = Fdco
							// (374 + 1) * 32768 = 12MHz
							// Set FLL Div = fDCOCLK/2
	_bic_SR_register(SCG0);

	P5DIR = 0;

	// Initialize the timers
	TA0CTL = TACLR;
	TA0CTL |= TASSEL_2 + MC_1 + ID__8;
	TA0EX0 |= TAIDEX_7;
	TA0CCTL1 |= OUTMOD_3;
	TA0CCTL2 |= OUTMOD_3;
	TA0CCTL3 |= OUTMOD_3;
	TA0CCTL4 |= OUTMOD_3;

	TA0CCR0 = 255;
	TA0CCR1 = 0;
	TA0CCR2 = 0;
	TA0CCR3 = 0;
	TA0CCR4 = 0;

	TA1CTL |= TACLR;
	TA1CTL |= TASSEL_2 + MC_1 + ID__8; //TAIE;
	TA1EX0 |= TAIDEX_7;
	TA1CCR0 = 256;

	TA2CTL = TACLR;

	// Initalize the LEDs
	P5DIR |= BIT1;
	PJDIR |= BIT3;

	P5OUT |= BIT1;
	PJOUT |= BIT3;

	// Initialize motor pins
	P1DIR = 0x3f;
	P1OUT = 0x3f;
	//P1SEL |= 0x3c;

	// Initialize sensor pins
	P1DIR |= BIT6;
	PJDIR |= BIT1 + BIT0;
	P2DIR |= BIT0;

	// Initialize UART
	P4SEL |= BIT5 + BIT4;
	UCA1CTL1 = UCSSEL_2 + UCSWRST;
	UCA1BR0 = 81; // previously 78
	UCA1BR1 = 0;
	UCA1MCTL = UCBRF_2 + UCOS16;
	UCA1CTL1 &= ~UCSWRST;
	UCA1IE |= UCRXIE;

	// Initialize ADC10
	P6DIR &= ~(BIT0 + BIT1);
	P6SEL |= BIT0 + BIT1;
	ADC10CTL0 &= ~ADC10ENC;
	ADC10MCTL0 = ADC10INCH_0;
	ADC10CTL1 = ADC10SHS_0 + ADC10DIV_1 + ADC10SSEL_0 + ADC10CONSEQ_0 + ADC10SHP;
	ADC10CTL2 = ADC10RES;
	ADC10CTL0 = ADC10SHT_0 + ADC10ON;

	// Initialize flags
	globals.isRunning = 0;
	globals.isConnected = 0;

	// Initialize the watchdog timer
	WDTCTL = WDT_MRST_32; // WDT 32ms, SMCLK, interval timer

	// Initialize Autopilot
	globals.disconnectedAutopilot.mode=MODE_VERTICAL|MODE_FORWARD|MODE_TURN;
	globals.disconnectedAutopilot.power=50;
	globals.connectedAutopilot.mode=MODE_VERTICAL|MODE_FORWARD|MODE_TURN;
	globals.connectedAutopilot.power=200;

	globals.connectedAutopilot.verticleSetPoint = 2700;
	globals.disconnectedAutopilot.verticleSetPoint = 2000;
	globals.pidVertSettings.p=-8;
	globals.pidVertSettings.i=-2;
	globals.pidVertSettings.d=-512;


	__bis_SR_register(GIE);
	// Enable interrupts
	globals.isInitialized = 1;
	return 0;
}

void set_vcore_up(unsigned int level) {
	// Open PMM registers for write
	PMMCTL0_H = PMMPW_H;
	// Set SVS/SVM high side new level
	SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level;
	// Set SVM low side to new level
	SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level;
	// Wait till SVM is settled
	while ((PMMIFG & SVSMLDLYIFG) == 0)
		;
	// Clear already set flags
	PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);
	// Set VCore to new level
	PMMCTL0_L = PMMCOREV0 * level;
	// Wait till new level reached
	if ((PMMIFG & SVMLIFG))
		while ((PMMIFG & SVMLVLRIFG) == 0)
			;
	// Set SVS/SVM low side to new level
	SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level;
	// Lock PMM registers for write access
	PMMCTL0_H = 0x00;
}
