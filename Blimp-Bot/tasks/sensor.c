/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#include "sensor.h"
#include "../globals.h"

#include "msp430f5510.h"

// Helper LED macros
#define TOGGLE_LED() P2OUT ^= BIT0
#define DISABLE_LED() P2OUT &= ~BIT0

// Number of samples to take for sensing
const int NUM_SAMPLES = 50;

static unsigned int counter = 0;
static int accumulator = 0;
static unsigned char direction = 0;

// Initial offsets for the sensor values. These are
// calibrated based on the surface used
static const int offsets[]={3400,3400,2045,1900,1800};

// Turns on the proper IR sensor
void setLight(unsigned char index) {

        switch(index) {
        case 0:
                P1OUT &= ~BIT6;
                PJOUT &= ~(BIT1 + BIT0);
                break;
        case 1:
                P1OUT |= BIT6;
                PJOUT &= ~(BIT1 + BIT0);
                break;
        case 2:
                P1OUT &= ~BIT6;
                PJOUT |= BIT0;
                PJOUT &= ~BIT1;
                break;
        case 3:
                P1OUT |= BIT6;
                PJOUT |= BIT0;
                PJOUT &= ~BIT1;
                break;
        case 4:
                P1OUT &= ~BIT6;
                PJOUT &= ~BIT0;
                PJOUT |= BIT1;
        }
}

int sensor_task(void *input) {
	switch (globals.sensingState) {
	case 0:
		if (!globals.activeSensor) {
			// Advance to use the next sensor
			globals.activeSensor = 1;
			globals.sensingDirection++;
			if (globals.sensingDirection>4) globals.sensingDirection=0;
			globals.sendSensorResultWhenDone = 0;
		}

		// Reset everything if we are not currently sensing
		accumulator=0;
		counter=0;

		if (globals.activeSensor) {
			// Turn on the current sensor and prepare it for use
			direction = globals.sensingDirection;
			globals.activeSensor = 0;
			setLight(direction);
			TOGGLE_LED();
			ADC10CTL0 |= ADC10SC + ADC10ENC;
			globals.sensingState=1;
		}
		break;

	case 1:
		if ( ADC10IFG & ADC10IFG0 ) {
			// Accumulate the measurements until we have the required number
			volatile int test = ADC10MEM0;
			if (counter&1) {
				accumulator -= test;
			} else {
				accumulator += test;
			}
			ADC10CTL0 |= ADC10SC + ADC10ENC;
			counter++;
			if (counter == NUM_SAMPLES) {
				// Indicate that sensing for the current sensor has finished
				// and record the final value of the sensor for use in other tasks
				DISABLE_LED();
				int fixed=(-accumulator)+offsets[direction];
				globals.smoothedRange[direction] -= globals.smoothedRange[direction]/16;
				globals.smoothedRange[direction] += fixed/8;
				globals.sensingState = 0;
				globals.sendSensorResult=globals.sendSensorResult||globals.sendSensorResultWhenDone;
				globals.sendSensorResultWhenDone = 0;
				globals.newSensorResult=1;
			} else {
				TOGGLE_LED();
			}
		}
		break;

	}
	return 0;
}
