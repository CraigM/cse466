/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#ifndef SYSMONITOR_H_
#define SYSMONITOR_H_

int sys_monitor (void*);

#endif
