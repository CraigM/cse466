/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#ifndef MOTIONCONTOLLER_H_
#define MOTIONCONTOLLER_H_

int motion_task (void*);

#endif
