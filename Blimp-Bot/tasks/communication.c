/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#include "communication.h"
#include "../globals.h"
#include "../buffer.h"
#include "../crc.h"

#include "msp430f5510.h"

const char FLAG = 'U';
const unsigned char PACKET_LENGTH = 16;
#define DATA_LENGTH 12
const unsigned char DATA_START = 1;
const unsigned char CRC_START = 13;

// Read a character, indexed from the current start of the input buffer
inline char read(unsigned char index){
	return bufferRead(&globals.serialIn,index);
}

// removes the first count bytes from input buffer
inline void consume(unsigned char count){
	bufferConsume(&globals.serialIn,count);
}

// does a packet start at start?
// 0 if no packet
// 1 if packet
char isPacket(unsigned char start){
	static int good=0;
	static int endOff=0;
	static int checkOff=0;
	// check for start flag
	if (read(start)!=FLAG) {
		return 0;
	}
	// check for end flag
	if (read(start+PACKET_LENGTH-1)!=FLAG) {
		endOff++;
		return 0;
	}
	// check CRC
	unsigned int running_crc = 0;
	unsigned char i;
	for (i=0; i<DATA_LENGTH; i++) {
		running_crc = crc(running_crc, read(start+i+DATA_START));
	}
	if ((read(CRC_START+start)==(char)running_crc) && (read(CRC_START+start+1)==(char)(running_crc>>8))) {
		good++;
		return 1; // CRC matched
	}
	checkOff++;
	return 0; // CRC mismatched
}

signed char findPacketData() {
	while (bufferedSize(&globals.serialIn)>=PACKET_LENGTH) {
		if (isPacket(0)) {
			return DATA_START;
		}
		// got garbage, thow it away
		consume(1);
	}
	return -1;
}

inline void removePacket() {
	consume(PACKET_LENGTH);
}


char roomForWitePacket(){
	return bufferedSize(&globals.serialOut)+PACKET_LENGTH<=BUFFER_SIZE;
}

// write a packet, check roomForWitePacket before calling this
// this could be implemented as a streaming 1 byte at a time write, but that is more complex,
// and would allow getting out of sync with the packet stream and produce lag on decode wiating for CRCs
// data is assumed to have length==DATA_LENGTH
void writePacket(char* data){
	// write start flag
	bufferWrite(&globals.serialOut,FLAG);
	
	// write data and compute crc
	unsigned int running_crc = 0;
	unsigned char i;
	for (i=0; i<DATA_LENGTH; i++) {
		bufferWrite(&globals.serialOut, data[i]);
		running_crc = crc(running_crc, data[i]);
	}
	
	// write crc, little endian
	bufferWrite(&globals.serialOut, (char)running_crc);
	bufferWrite(&globals.serialOut, (char)(running_crc>>8));
	
	// write end flag
	bufferWrite(&globals.serialOut,FLAG);
}


char sendMotionResponse=0;
char sendPing=0;
char sendStatusResponse=0;

int readInt(index){
	return (int)(((unsigned int)read(index))|(((unsigned int)read(index+1)))<<8);
}

// returns length of message, including type byte
// return 0 to throw away packet (such as the case of an unknown message, or no more messages)
unsigned char handelMessage(unsigned char messageType, unsigned char index){
	switch (messageType) {
		
		// RUN/STOP controller signals
		case 1: // RUN
			globals.isRunning=1;
			return 1;
		case 2: // STOP
			globals.isRunning=0;
			return 1;
		
		// Low-level motor control commands
		case 11: // LL_SET
			{
				unsigned char motorIndex=read(index);
				unsigned char direction=read(index+1);
				unsigned char power=read(index+2);
				globals.motor[motorIndex].direction=direction;
				globals.motor[motorIndex].power=power;
				return 4;
			}
		// High-level motion control
		case 21: // HL_SET
			return 2;
		case 22: // TUNE
			globals.connectedAutopilot.verticleSetPoint = readInt(index);
			globals.pidVertSettings.p= readInt(index+2);
			globals.pidVertSettings.i= readInt(index+4);;
			globals.pidVertSettings.d= readInt(index+6);;
			return 9;
		case 23: // QUERY_MOTION
			sendMotionResponse=1;
			return 1;
		
		// Sensing
		case 31: // POLL_SENSOR
			globals.activeSensor = 1;
			globals.sensingDirection = read(index);
			globals.sendSensorResultWhenDone=1;
			return 2;
		
		// System Monitor
		case 41: // PING
			sendPing=1;
			return 1;
		case 42: // QUERY_STATUS
			sendStatusResponse=1;
			return 1;
		
		case 43: // RESET
			// Cause a reset:
			WDTCTL = 0; // Goodby world
			return 1; // Ya, this is never gonna run, but whatever.
		
		// Set manual controls
		case 81: // manualControlMask
			globals.manualControlMask=read(index);
			return 2;

		// Outgoing
		case 51: // MOTION_RESPONSE
		case 61: // SENSOR_RESULT
		case 71: // PING_RESPONSE
		case 72: // STATUS_RESPONSE
			return 0; // These are commands we should be sending, not getting!
	}
	return 0;
}

void sendFromBuffer(){
	// Write output from buffer to serial port
	if ((UCA1IFG & UCTXIFG) && (bufferedSize(&globals.serialOut)>0)) {
		// USCI_A1 TX buffer is ready, and we have data to send
		char to_send = bufferRead(&globals.serialOut, 0);
		UCA1TXBUF = to_send;
		bufferConsume(&globals.serialOut,1);
		UCA1IFG&=!UCTXIFG; // This is needed, apparently.
	}
}

const unsigned int CONNECTION_TIMEOUT = 30000;
unsigned int sincePacket = 0;

int communication_task (void *input){
	sincePacket++;
	if (sincePacket==CONNECTION_TIMEOUT) {
		globals.isConnected=0;
	}
	
	sendFromBuffer();
	
	// Process input from serial buffer
	signed char start;
	while (0<=(start=findPacketData())) {
		globals.isConnected=1;
		sincePacket=0;
		// a new packet starts at start
		// process the packet
		unsigned char messageStart=0;
		// a packet may contain multiple messages, so loop over all possible messages
		while (messageStart<DATA_LENGTH) {
			unsigned char messageType=read(start+messageStart);
			unsigned char messageLength=handelMessage(messageType,start+messageStart+1);
			if (messageLength == 0){
				// invalid message (no knowen legnth, skip rest of packet)
				goto FinishPacket;
			}
			messageStart+=messageLength;
		}
	FinishPacket:
		removePacket();
		
		// That may have taken a while, so lets try sending again.
		// This is just a hack to hopefully send more often,
		// and not have bad sending lag when dealing with a lot of input.
		// Properly enabling and disabling a sending interrupt would be better (faster),
		// but this is really easy, and unlikley to have race conditions or other bugs.
		//sendFromBuffer();
	}
	// no more packets
	

	if ((sendPing || globals.sendSensorResult) && roomForWitePacket()) {
		char message[DATA_LENGTH]={61,0,0,0,0,0,0,0,0,0,0,71};
		int i;
		for (i=0;i<5;i++){
			volatile unsigned int range=globals.smoothedRange[i];
			volatile unsigned char low = (char) (range & 0xFF);
			volatile unsigned char high = (char) ((range >> 8) & 0xFF);
			volatile int messageindex = 1+2*i;
            message[messageindex] = low;
            message[messageindex+1] = high;
		}
		writePacket(&message[0]);
		globals.sendSensorResult=0;
		sendPing=0;
	}
	if (sendMotionResponse && roomForWitePacket()) {
		char motion = 0;
		char message[DATA_LENGTH]={51,0,71,71,71,71,71,71,71,71,71,71};
		message[1]=motion;
		writePacket(&message[0]);
		sendMotionResponse=0;
	}
	if (sendStatusResponse && roomForWitePacket()) {
		char status = 0;
		if (globals.isRunning) {
			status|=1;
		}
		char message[DATA_LENGTH]={72,0,71,71,71,71,71,71,71,71,71,71};
		message[1]=status;
		writePacket(&message[0]);
		sendStatusResponse=0;
	}
	return 0;
}

#pragma vector = USCI_A1_VECTOR
__interrupt void USCI_UART_ISR (void){
	// write character to input buffer
	if (UCRXIFG & UCA1IFG)
	bufferTryWrite(&globals.serialIn,UCA1RXBUF);
}

