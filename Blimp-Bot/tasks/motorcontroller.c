/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#include "motorcontroller.h"
#include "../globals.h"

#include "msp430f5510.h"

// Duty cycles for the left and right motors
#define RFAN_H_DUTY TA0CCR1
#define RFAN_L_DUTY TA0CCR2
#define LFAN_H_DUTY TA0CCR3
#define LFAN_L_DUTY TA0CCR4

// Bits for each of the motors
#define VFAN_H 0x1
#define VFAN_L 0x2
#define RFAN_H 0x4
#define RFAN_L 0x8
#define LFAN_H 0x10
#define LFAN_L 0x20

// Flag whether vertical fan is supposed to be going forward
int vfan_forward = 0;

// Set the given pin high
static void setPinHigh(int pin) {
	P1SEL &= ~pin;
	P1DIR |= pin;
}

int motor_task (void *input) {

	if (!globals.isRunning) {
		// Disable all of the motors if we're not running
		TA1CCTL0 &= ~CCIE;
		TA1CCTL1 &= ~CCIE;
		setPinHigh(LFAN_H + LFAN_L + RFAN_H + RFAN_L);
		P1DIR |= VFAN_H + VFAN_L;
	} else {
		if (globals.motor[VERTICAL].power == 0) {
			// Disable the vertical motor if the user has set that
			TA1CCTL0 &= ~CCIE;
			TA1CCTL1 &= ~CCIE;
			P1DIR |= VFAN_H + VFAN_L;
		} else {
			// Set the desired duty cycle for the vertical motor
			TA1CCR1 = globals.motor[VERTICAL].power;

			// Set the proper direction of the vertical motor
			if (globals.motor[VERTICAL].direction == FORWARD) {
				P1DIR |= VFAN_L;
				vfan_forward = 1;
			} else {
				P1DIR |= VFAN_H;
				vfan_forward = 0;
			}

			// Enable interrupts for the vertical timers
			TA1CCTL0 |= CCIE;
			TA1CCTL1 |= CCIE;
		}


		if (globals.motor[LEFT].power == 0) {
			// Turn off the left motor if specified by user
			setPinHigh(LFAN_H + LFAN_L);
		} else {
			if (globals.motor[LEFT].direction == FORWARD) {
				// Set the left motor to go forward
				setPinHigh(LFAN_H);
				LFAN_L_DUTY = globals.motor[LEFT].power;
				P1SEL |= LFAN_L;
			} else {
				// Set the left motor to go backward
				setPinHigh(LFAN_L);
				LFAN_H_DUTY = globals.motor[LEFT].power;
				P1SEL |= LFAN_H;
			}
		}

		if (globals.motor[RIGHT].power == 0) {
			// Turn off the right motor if specified by user
			setPinHigh(RFAN_H + RFAN_L);
		} else {
			if (globals.motor[RIGHT].direction == FORWARD) {
				// Set the right motor to go forward
				setPinHigh(RFAN_H);
				RFAN_L_DUTY = globals.motor[RIGHT].power;
				P1SEL |= RFAN_L;
			} else {
				// Set the right motor to go backward
				setPinHigh(RFAN_L);
				RFAN_H_DUTY = globals.motor[RIGHT].power;
				P1SEL |= RFAN_H;
			}
		}
	}

	return 0;
}

// The below interrupt handlers simulate a PWM for the vertical
// motor since we only have 2 PWMs for the left and right motors
#pragma vector = TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void) {
	if (vfan_forward) {
		P1DIR &= ~VFAN_H;
	} else {
		P1DIR &= ~VFAN_L;
	}
}

#pragma vector = TIMER1_A1_VECTOR
__interrupt void TIMER1_A1_ISR(void) {
	if (vfan_forward) {
		P1DIR |= VFAN_H;
	} else {
		P1DIR |= VFAN_L;
	}
	TA1IV &= ~TA1IV_TA1CCR1;
}
