/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#include "tasks.h"
#include "tcb/tcb.h"
#include "tasks/sysinit.h"
#include "tasks/sysmonitor.h"
#include "tasks/communication.h"
#include "tasks/motorcontroller.h"
#include "tasks/motioncontroller.h"
#include "tasks/sensor.h"

// An array of tasks
TCB tasks[NUM_TASKS]={
	{(task_func)sys_init,0},
	{(task_func)sys_monitor,0},
	{(task_func)communication_task,0},
	{(task_func)sensor_task,0},
	{(task_func)motion_task,0},
	{(task_func)motor_task,0},
};
