/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#ifndef PID_H_
#define PID_H_

typedef struct {
	int p;
	int i;
	int d;
} PIDSettings;

typedef struct {
	PIDSettings *settings;
	int integral;
	int lastError;
} PID;

int updatePID(PID *pid, int setPoint, int value);

#endif // PID_H_
