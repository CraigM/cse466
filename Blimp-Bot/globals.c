/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#include "globals.h"
#include "buffer.h"

Globals globals={
	0,
	0,
	0,
	0,
	{0,0,0,0,0},
	{{0,0},{0,0},{0,0}},
	0,
	0,
	0,
	0,
	0,
	0,
	{0,0,0},
	{0,0,0},
	0,
	{0,0,0},
	{0,0},
	{0,0},
};

void writeGlobalChar(unsigned index, char data){
	((char *)&globals)[index]=data;
}

char readGlobalChar(unsigned index){
	return ((char *)&globals)[index];
}
