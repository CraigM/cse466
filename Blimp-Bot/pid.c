/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#include "pid.h"

const int MAX_INTEGRAL=2048;
const int INTEGRAL_DIV=16;

#define MAX_ERROR 1024

// 2^16/4
#define MAX_DELTA_ERROR 16384

// Max Values to avoid overflow: 16 8 2048

int updatePID(PID *pid, int setPoint, int value) {
	int error=value-setPoint;
	if (error>MAX_ERROR) {
		error=MAX_ERROR;
	} else if (error<-MAX_ERROR) {
		error=-MAX_ERROR;
	}
	pid->integral+=error/INTEGRAL_DIV;
	// Crude way to avoid overflow (usally)
	if (pid->integral>MAX_INTEGRAL) {
		pid->integral=MAX_INTEGRAL;
	} else if (pid->integral<-MAX_INTEGRAL) {
		pid->integral=-MAX_INTEGRAL;
	}
	int deltaError=error-(pid->lastError);
	int deltaErrorScale=deltaError>>4;
	deltaErrorScale=(pid->settings->d)>>4;
	deltaErrorScale =(deltaErrorScale>0?deltaErrorScale:-deltaErrorScale);

	if (deltaErrorScale>(MAX_DELTA_ERROR>>8)) {
		deltaError=deltaError>0?MAX_DELTA_ERROR:-MAX_DELTA_ERROR;
	} else {
		deltaError*=pid->settings->d;
	}

	pid->lastError=error;
	int p,i,d;
	p=(pid->settings->p)*error;
	i=(pid->settings->i)*(pid->integral);
	d=(pid->settings->d)*deltaError;
	return p+i+d;
}
