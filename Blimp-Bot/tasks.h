/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#ifndef TASKS_H_
#define TASKS_H_

#include "tcb/tcb.h"

#define NUM_TASKS 6

extern TCB tasks[NUM_TASKS];

#endif // TASKS_H_
