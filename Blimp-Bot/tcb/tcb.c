/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#include "tcb.h"

int exec_next_task(TaskQueuePtr queue, int *ret) {
    if (!queue) {
        return 1;
    }
    TCB task_struct = queue->tasks[queue->current];
    *ret = task_struct.task(task_struct.data);
    queue->current++;
    if (queue->current == queue->len) {
        queue->current = 0;
    }
    return 0;
}
