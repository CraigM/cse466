/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#ifndef TCB_H_
#define TCB_H_

#include <stdint.h>

typedef int (*task_func)(void*);
typedef void *task_data;

// Represents a task. Contains function and data pointers.
typedef struct {
    task_func task;
    task_data data;
} TCB, *TCBPtr;

// Represents a queue of tasks.
typedef struct {
    TCB *tasks;
    uint8_t len;
    uint8_t current;
} TaskQueue, *TaskQueuePtr;

// Executes the next task in the task queue. Returns 0 if the task was called,
// and returns the tasks's result through the ret parameter.
int exec_next_task(TaskQueuePtr queue, int *ret);

#endif // TCB_H_
