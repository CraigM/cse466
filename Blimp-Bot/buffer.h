/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#ifndef BUFFER_H_
#define BUFFER_H_

// Herein lives Craig's Circular Buffer
// This buffer should be safe to read and write from mainline
// as well as interrupts as long as there is only one reader and one writer.

#define BUFFER_SIZE 32

typedef struct {
	unsigned char start;
	unsigned char dataLength;
	char data[BUFFER_SIZE];
} Buffer, *PBuffer;

// Read a character, indexed from the current start of the input buffer
char bufferRead(PBuffer b, unsigned char index);

// The ammount in the input buffer
int bufferedSize(PBuffer b);

// removes the first count bytes from input buffer
void bufferConsume(PBuffer b, unsigned char count);

// Writes into the buffer
void bufferWrite(PBuffer b, char data);

// writes into the buffer if there is space, else throws away data
void bufferTryWrite(PBuffer b, char data);

#endif // BUFFER_H_
