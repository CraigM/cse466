/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

// Herein lives Craig's Circular Buffer
// This buffer should be safe to read and write from mainline
// as well as interrupts as long as there is only one reader and one writer.

#include "buffer.h"

#include "msp430f5510.h"

#define BUFFER_LOCK unsigned int _oldStatus = __bic_SR_register(GIE)
#define BUFFER_UNLOCK if (_oldStatus & GIE) __bis_SR_register(GIE)

inline char* indexBuffer(PBuffer b, unsigned char index){
	return &((b->data)[((b->start)+index)%BUFFER_SIZE]);
}

// Read a character, indexed from the current start of the input buffer
char bufferRead(PBuffer b, unsigned char index){
	return *indexBuffer(b,index);
}

// The ammount in the input buffer
int bufferedSize(PBuffer b){
	return b->dataLength;
}

// removes the first count bytes from input buffer
void bufferConsume(PBuffer b, unsigned char count){
	BUFFER_LOCK;
	b->dataLength-=count;
	b->start=(b->start+count)%BUFFER_SIZE;
	BUFFER_UNLOCK;
}

// Writes into the buffer
void bufferWrite(PBuffer b, char data){
	BUFFER_LOCK;
	*indexBuffer(b,b->dataLength)=data;
	(b->dataLength)++;
	BUFFER_UNLOCK;
}


// writes into the buffer if there is space, else throws away data
void bufferTryWrite(PBuffer b, char data){
	if ((b->dataLength)<BUFFER_SIZE) {
		bufferWrite(b,data);
	}
}
