/*
 * Will Pitts, Sean Cowan, Alexandre Bykov, Craig Macomber
 * CSE 466 Final project
 * [IR] LED Zeppelin
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include "buffer.h"
#include "pid.h"

#define VERTICAL 0
#define LEFT 1
#define RIGHT 2
#define FORWARD 1
#define REVERSE 0 

#define RANGE_FORWARD 0
#define RANGE_REVERSE 1
#define RANGE_RIGHT 2
#define RANGE_LEFT 3
#define RANGE_DOWN 4


typedef struct {
	char direction;
	unsigned char power;
} MotorSetting;

#define MODE_NONE 0
#define MODE_VERTICAL 1
#define MODE_FORWARD 2
#define MODE_TURN 4

#define MODE_HORIZONTAL 2

typedef struct {
	char mode;
	unsigned char power;
	int verticleSetPoint;
} Autopilot;


typedef struct {
	char isInitialized;
    char isRunning;
    char isConnected;
    int temperature;
	int smoothedRange[5];
	MotorSetting motor[3];
	unsigned char activeSensor;
    unsigned char sensingDirection;
	unsigned char sensingState;
	unsigned char sendSensorResult;
	unsigned char sendSensorResultWhenDone;
	unsigned char newSensorResult;
	Autopilot connectedAutopilot;
	Autopilot disconnectedAutopilot;
	unsigned char manualControlMask; //Like MODE, Bit0=Vertical Bit1=Horizontal
	PIDSettings pidVertSettings;
	Buffer serialIn;
	Buffer serialOut;
} Globals, *Globals_ptr;




extern Globals globals;

void writeGlobalChar(unsigned index, char data);

char readGlobalChar(unsigned index);

#endif // GLOBALS_H_
